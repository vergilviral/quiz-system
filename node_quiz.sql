-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 08, 2017 at 07:48 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `node_quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `fbstatus`
--

CREATE TABLE `fbstatus` (
  `status_id` int(11) NOT NULL,
  `s_text` text,
  `t_status` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fbstatus`
--

INSERT INTO `fbstatus` (`status_id`, `s_text`, `t_status`) VALUES
(1, 'hey', '2017-09-06 17:01:19'),
(2, 'hey', '2017-09-06 17:01:20'),
(3, 'hey', '2017-09-06 17:01:20'),
(4, 'hey', '2017-09-06 18:01:44'),
(5, 'hey', '2017-09-06 18:01:52'),
(6, 'hey', '2017-09-06 18:01:52'),
(7, 'hey', '2017-09-06 18:01:53'),
(8, 'why not working?', '2017-09-06 18:01:59'),
(9, 'now working :)', '2017-09-06 18:02:08'),
(10, 'ssup?', '2017-09-06 18:02:21'),
(11, '', '2017-09-06 18:05:28'),
(12, 'hey', '2017-09-06 18:05:30');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answers`
--

CREATE TABLE `quiz_answers` (
  `ans_id` int(2) NOT NULL,
  `answer` varchar(200) NOT NULL,
  `ques_id` int(11) NOT NULL,
  `counter` int(2) NOT NULL DEFAULT '0',
  `is_true` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz_answers`
--

INSERT INTO `quiz_answers` (`ans_id`, `answer`, `ques_id`, `counter`, `is_true`) VALUES
(1, '4', 1, 0, 1),
(2, '3+1', 1, 0, 1),
(3, '18', 2, 0, 1),
(4, '17+2', 2, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_questions`
--

CREATE TABLE `quiz_questions` (
  `id` int(2) NOT NULL,
  `question` varchar(100) NOT NULL,
  `img_path` varchar(200) NOT NULL,
  `is_multi` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz_questions`
--

INSERT INTO `quiz_questions` (`id`, `question`, `img_path`, `is_multi`) VALUES
(1, 'what is 2+2?', '', 1),
(2, 'what is 9+9?', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `u_id` int(2) NOT NULL,
  `username` varchar(50) NOT NULL,
  `timestamp` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `socket_id` varchar(50) NOT NULL,
  `role` varchar(10) NOT NULL DEFAULT 'student',
  `score` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`u_id`, `username`, `timestamp`, `socket_id`, `role`, `score`) VALUES
(148, 'viral', '2017-09-08 04:57:35.008659', 'b0g_pPXN5g_CuLbIAAAN', 'student', 0),
(164, 'viral', '2017-09-08 05:13:57.485693', 'ZsgiJEvSkNnjRcbyAAAQ', 'student', 0),
(169, 'v', '2017-09-08 05:17:41.912585', 'YP9o9Ej0-4TioSEuAAAB', 'student', 0),
(171, 'viral', '2017-09-08 05:19:13.803781', '0KgB8SjDchaIBv9PAAAB', 'student', 100),
(172, 'v', '2017-09-08 05:20:28.508494', '91b4bup7gYJ-CSV7AAAB', 'student', 100),
(174, 'v', '2017-09-08 05:26:25.664197', 'vs1LpgpEnOPnJDUwAAAC', 'student', 0),
(177, 'v', '2017-09-08 05:28:07.584634', 'OgEOMolC7DLFvHMaAAAD', 'student', 100),
(179, 'v', '2017-09-08 05:29:58.647717', '8LE1C6IdFlTM5SNyAAAC', 'student', 0),
(181, 'v', '2017-09-08 05:31:03.502857', '26Frmisut9eci6TbAAAC', 'student', 100),
(182, 'v', '2017-09-08 05:33:19.962038', 'bms6EFb78f_Hcj1QAAAB', 'student', 0),
(183, 'v', '2017-09-08 05:35:11.066224', 'STeixmUebOMvZYdeAAAB', 'student', 100),
(184, 'v', '2017-09-08 05:37:40.059269', '8ORt9mlA6Bs1B6-8AAAB', 'student', 100),
(186, 'c', '2017-09-08 05:39:44.840052', '2Q8x8PutyRxqeBV3AAAC', 'student', 100),
(187, 'v', '2017-09-08 05:40:06.271361', 'OXPTwH_FNyV0bvehAAAB', 'student', 100),
(188, 'v', '2017-09-08 05:42:45.117304', 'jg-D8GDOShbEGJITAAAA', 'student', 100),
(189, 'v', '2017-09-08 05:43:32.502049', '8C-HYtCInB652NCLAAAB', 'student', 200),
(190, 'viral', '2017-09-08 05:46:09.909835', 'McRhxagXdBg_h1c8AAAA', 'student', 100),
(191, 'mitul', '2017-09-08 05:46:12.728147', 'CnH-jaCIPDIMr6vwAAAB', 'student', 100),
(193, 'v', '2017-09-08 05:48:58.044144', 'fBcATSXDLMmB-YBdAAAD', 'student', 0),
(195, 'v', '2017-09-08 05:49:45.909690', 'OCCr4djnhJRurUavAAAC', 'student', 0),
(197, 'v', '2017-09-08 05:50:34.820280', 'IypQRldSHebx6FkDAAAD', 'student', 0),
(199, 'viral', '2017-09-08 06:49:10.385776', 'IsJf9SRyu5eqXnyFAAAB', 'student', 100),
(200, 'mitul', '2017-09-08 06:49:15.183561', 'OditNkMh8ZncXE5bAAAC', 'student', 100);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fbstatus`
--
ALTER TABLE `fbstatus`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `quiz_answers`
--
ALTER TABLE `quiz_answers`
  ADD PRIMARY KEY (`ans_id`);

--
-- Indexes for table `quiz_questions`
--
ALTER TABLE `quiz_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`u_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fbstatus`
--
ALTER TABLE `fbstatus`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `quiz_answers`
--
ALTER TABLE `quiz_answers`
  MODIFY `ans_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `quiz_questions`
--
ALTER TABLE `quiz_questions`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `u_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
