var gulp = require('gulp');

// For concatenation and minification of CSS and JS
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');

gulp.task('useref', function(){
    return gulp.src('*.html')
        .pipe(useref())
        .pipe(gulpIf('*.js', uglify()))
        // Minifies only if it's a CSS file
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest('dist'))
});

gulp.task('userefonfront', function(){
    return gulp.src('home.html')
        .pipe(useref())
        .pipe(gulpIf('*.js', uglify()))
        // Minifies only if it's a CSS file
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest('dist'))
});

// For Image Optimization and caching

var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');

gulp.task('images', function(){
    return gulp.src('assets/images/**/*.+(png|jpg|jpeg|gif|svg)')
    // Caching images that ran through imagemin
        .pipe(cache(imagemin()))
        .pipe(gulp.dest('dist/images'))
});

// Copying fonts from assets to dist (for deployment)

gulp.task('fonts', function() {
    return gulp.src('assets/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'))
})

// Clean up old files before making new ones

var del = require('del');

gulp.task('clean:dist', function() {
    return del.sync('dist');
})

// Run all tasks in sequence

var runSequence = require('run-sequence');

gulp.task('build', function (callback) {
    runSequence('clean:dist',
        ['useref', 'userefonfront', 'images', 'fonts'],
        callback
    )
})
