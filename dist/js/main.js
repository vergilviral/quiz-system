$(document).ready(function () {


    if (window.location.href.split('#')[0] != window.location.href)
        window.location.href = window.location.href.split('#')[0];

    var ns = getUrlVars()["ns"];

    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
            function (m, key, value) {
                vars[key] = value;
            });
        return vars;
    }

    if (!ns) {
        window.location.replace("http://vedaquiz.com/");
    }
    else {
        $("#body").fadeIn(550, function () {
            // Animation complete
        });
        document.title = 'Welcome to ' + ns;
    }
    var socket = io();
    var next_q_avail = 1;
    var correct_given = 0;
    var incorrect_given = 0;
    var image;
    var teacher = false;
    var is_correct = false;
    var quiz_type = '';
    var ranks = [];
    var last_rank = -1;
    var total_qs = 0;

    socket.on('ping', function (data) {
        // console.log('received ping' + ', now sending pong');
        socket.emit('pong');
    });

    socket.on('callback for required files', function (data) {
        socket.emit('add all required files');
    });

    var q_id = 1;
    $("#user_new").fadeOut("slow", function () {
        // Animation complete
    });
    $("#loginas").fadeIn(550, function () {
        // Animation complete
    });
    $("#username").fadeOut("slow", function () {
        // Animation complete
    });
    $("#passcodeforadmin").fadeOut("slow", function () {
        // Animation complete
    });

    //login as teacher
    $("#asteacher").submit(function () {
        $("#asteacher").remove();
        $("#asstudent").remove();
        $("#username").remove();
        $("#loginoptions").remove();
        $("#loginas").addClass('vertical-middle background-light med-padding');
        $("#body").addClass('teacherbody');
        // $("#body").css('background-image', 'url("assets/images/teacherfull.jpg")');
        //showloader();
        $("#passcodeforadmin").fadeIn(550, function () {
            // Animation complete
        });
        $("#passcodegrid").append('<div class="mdl-cell mdl-cell--4-col">\n' +
            '                    <input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"\n' +
            '                           type="submit" value="Submit"/>\n' +
            '                </div>');
        $("#helptext").html('Please enter your account credentials. <br/><br/><b>Note:</b> If you have multiple accounts then please use the account with which you created this quiz.')
        return false;
    });

    //login as student
    $("#asstudent").submit(function () {
        $("#asteacher").remove();
        $("#asstudent").remove();
        $("#passcodeforadmin").remove();
        $("#loginoptions").remove();
        $("#loginas").addClass('vertical-middle background-light med-padding');
        $("#body").addClass('studentbody');
        //showloader();
        $("#username").fadeIn(550, function () {
            // Animation complete
        });
        $("#usernamegrid").append('<div class="mdl-cell mdl-cell--4-col">\n' +
            '                    <input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"\n' +
            '                           type="submit" value="Join"/>\n' +
            '                </div>');
        $("#helptext").html('Please enter valid username. <br/><br/>Username can be numbers of alphabets but it should be unique.')
        return false;
    });

    //verify password of teacher
    $("#passcodeforadmin").submit(function () {
        //showloader();
        if ($("#tname").val() && $("#code").val()) {
            //console.log('came to event');
            socket.emit("verify password", {tname: $("#tname").val(), code: $("#code").val()});
            return false;
        }
        else {
            $(".notice").empty();
            $("#empty_name").css('display', 'none');
            $("#empty_name").html('<span class="noticespan">Name and Password can not be empty.</span>');
            $("#empty_name").slideDown("slow", function () {
                // Animation complete.
            });
            return false;
        }
    });

    // add username for non-admin users
    $("#username").submit(function () {
        //showloader();
        socket.emit('add student', $("#uname").val());
        return false;
    });

    // users disconnected
    socket.on('disconnected user', function (username) {
        (function () {
            'use strict';
            // var snackbarContainer = document.querySelector('#snackbar');
            // var showSnackbarButton = document.querySelector('#show-snackbar');
            // var handler = function (event) {
            //     showSnackbarButton.style.backgroundColor = '';
            // };
            var notification = document.querySelector('.mdl-js-snackbar');
            notification.MaterialSnackbar.showSnackbar(
                {
                    message: username + ' Left :('
                }
            );
        }());
        $("#" + username + "connected").remove();
    });

    // student connected message
    socket.on('student connected', function (username) {
        if (!$('#loginas').length && $('#waiting').length) {
            $("#error_no_student_left").empty();
            $("#user_new").fadeIn(550, function () {
                // Animation complete
            });
            // $("#user_new").append('<span class="mdl-chip connectedstu" id="' + username + 'connected">\n' +
            //     '    <span class="mdl-chip__text">' + username + '</span>\n' +
            //     '</span>');

            $("#user_new").append('<button class="mdl-chip leader-chip connectedstu" id="' + username + '">\n' +
                '                <span class="mdl-chip__contact" style=\'background-image: url("https://robohash.org/' + username + '?size=50x50&set=set3");\'>&nbsp;</span>\n' +
                '            <span class="mdl-chip__text leader-username">' + username + '</span>\n' +
                '            </button>');

            (function () {
                'use strict';
                // var snackbarContainer = document.querySelector('#snackbar');
                // var showSnackbarButton = document.querySelector('#show-snackbar');
                // var handler = function (event) {
                //     showSnackbarButton.style.backgroundColor = '';
                // };
                var notification = document.querySelector('.mdl-js-snackbar');
                notification.MaterialSnackbar.showSnackbar(
                    {
                        message: username + ' Joined as Student'
                    }
                );
            }());
        }
        else {
            // student hasn't logged in or
            // student has passed the waiting screen and now inside the quiz);
        }
    });

    socket.on('showwaiting', function (username) {
        socket.emit("waitingnow others");
    });

    // teacher connected message
    socket.on('teacher connected', function (data) {
        //console.log('type' + data.type);
        if (data.type) {
            quiz_type = data.type;
        }
        $("#passcodeforadmin").fadeOut("slow", function () {
            $("#username_pass_no_match").empty();
            $("#error_no_teacher_left").empty();
            $("#body").removeClass('teacherbody');
            $("#user_new").fadeIn(550, function () {
                if (data.id != 'NULL')
                    $("#user_new").append('<h3>Connect here: http://qveda.co/' + data.id + '</h3>');
            });
            // $("#user_new").append('<span class="mdl-chip connectedstu" id=' + username + 'connected">\n' +
            //     '    <span class="mdl-chip__text">' + username + '</span>\n' +
            //     '</span>');
            (function () {
                'use strict';
                // var snackbarContainer = document.querySelector('#snackbar');
                // var showSnackbarButton = document.querySelector('#show-snackbar');
                // var handler = function (event) {
                //     showSnackbarButton.style.backgroundColor = '';
                // };
                var notification = document.querySelector('.mdl-js-snackbar');
                notification.MaterialSnackbar.showSnackbar(
                    {
                        message: data.name + ' Joined as Teacher'
                    }
                );
            }());
            $("#helptext").html('You can start quiz from this screen by clicking on "Start Quiz" button. You can also see all connected users in this screen.<br/><br/><b>Note:</b> You can not start until atleast one participant joins this quiz.')
            socket.emit("show connected users and start screen");
        });
    });

    // waiting for other players shown to students
    socket.on("waitingnow", function () {
        $("#empty_name").empty();
        $("#wait_for_teacher").empty();
        $("#username_taken").empty();
        $("#loginas").remove();
        $("#body").removeClass('studentbody');
        $("#waiting").fadeIn(550, function () {
            // Animation complete
        });
        $("#waiting").prepend("<h1>Waiting for other players</h1>");
        $("#helptext").html('You can see all connected users in this screen. Here you can wait for other participants to join until lecturer starts the quiz.')
    });

    //show data before quiz i.e. connected users and start quiz button to admin
    socket.on('show data before quiz', function (data) {
        $("#loginas").remove();
        for (var i = 0; i < data.length; i++)
            $("#connectedusers").append(data[i].username);
        $("#startquizscreen").fadeIn(550, function () {
            // Animation complete
        });
        $("#connectedusers").fadeIn(550, function () {
            // Animation complete
        });
    });

    // just display the start quiz button now after 1st user connect
    socket.on('display start screen to teacher now', function (data) {
        $("#loginas").remove();
        // $("#no_users").remove();
        $("#no_users").slideUp("slow", function () {
            // Animation complete.
        });

        $("#startquizscreen").fadeIn(550, function () {
            // Animation complete
        });

        if (data) {
            //console.log('is data: ' + data);
            if (data.all_questions) {
                //console.log('is data.allquestions length: ' + data.all_questions.length);
                if (quiz_type != "FixedTime") {
                    $("#jumptolinks").html('<div class="mdl-sheet__container">\n' +
                        '    <div class="mdl-sheet mdl-shadow--2dp jumptosheet">\n' +
                        '        <i class="material-icons mdl-sheet__icon">redo</i>\n' +
                        '\n' +
                        '        <div class="mdl-sheet__content" id="jumptotext">\n' +
                        '        <h5>Jump to any question from here:</h5><br/></div>\n' +
                        '    </div>\n' +
                        '</div>');
                    for (var i = 0; i < data.all_questions.length; i++) {
                        $("#jumptotext").append('<a class="jumpto" href="#" id="' + i + '">' + (i + 1) + ': ' + data.all_questions[i].question + '</a><br/><br/>');
                    }
                    var $jumpsheet = $('.jumptosheet');

                    if ($jumpsheet.length > 0) {
                        $('html').on('click', function () {
                            $jumpsheet.removeClass('mdl-sheet--opened')
                        });

                        $jumpsheet.on('click', function (event) {
                            event.stopPropagation();

                            $jumpsheet.addClass('mdl-sheet--opened');
                        });
                    }

                    $(".jumpto").click(function (event) {
                        event.preventDefault();
                        $("#singleqa").removeClass('qapop');
                        $("#startquizscreen").remove();
                        $("#waiting").remove();
                        $("#nextquestion").fadeOut("slow", function () {
                            $("#singleqa").fadeIn(550, function () {
                                // Animation complete
                            });
                        });
                        $('.jumptosheet').removeClass('mdl-sheet--opened');
                        //showloader();
                        $(this).addClass('strike');
                        var jumpTo = parseInt($(this).attr('id'));


                        //console.log(jumpTo);

                        socket.emit("dividequiz screens", {jump_q: jumpTo});
                        //console.log($(this).attr('id'));
                    });
                }
            }
        }

        $("#startquizscreentitle").append('<h1>Click on start quiz button to start quiz</h1><br/><br/>');
        $("#startq").append('<div class="mdl-cell mdl-cell--3-col"><input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent pulse" type="submit" value="Start Quiz" /></div>');
    });


    //show data before quiz when no student has yet joined i.e. connected users and start quiz button to admin
    socket.on('show data before quiz for no students joined', function (data) {
        $("#loginas").remove();
        $("#startquizscreen").fadeOut("slow", function () {
            $(".notice").empty();
            $("#no_users").css('display', 'none');
            $("#no_users").append('<span class="noticespan">You need at least one student to be logged in before starting the quiz.</span>');
            $("#no_users").slideDown("slow", function () {
                // Animation complete.
            });
            $("#connectedusers").fadeIn(550, function () {

            });
        });

    });

    // start quiz pressed by teacher
    $("#startq").submit(function () {
        $("#startquizscreen").remove();
        $("#waiting").remove();
        if (quiz_type == 'FixedTime') {
            $("#resultsscreen").addClass('vertical-middle');
            $("#resultsscreen").css('height', 'auto');
            $("#nextquestion").fadeOut("slow", function () {
                $("#resultsscreen").html('<table id="rtstatstable" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">' +
                    '<thead>\n' +
                    '    <tr>\n' +
                    '      <th>Question Number</th>\n' +
                    '      <th>Correct Answers Given</th>\n' +
                    '      <th>Partial Correct Answers Given</th>\n' +
                    '      <th>Incorrect Answers Given</th>\n' +
                    '      <th class="mdl-data-table__cell--non-numeric">Fastest Answer Given By</th>\n' +
                    '      <th class="mdl-data-table__cell--non-numeric">Toughest Question</th>\n' +
                    '    </tr>\n' +
                    '</thead>' +
                    '<tbody id="rtstatscontent"></tbody>' +
                    '</table>');
                $("#resultsscreen").append('<div id="liveleaderboard"></div>');
                $("#liveleaderboard").append('<h3 class="topmar">Leader Board</h3>');

                $("#user_new").remove();
                socket.emit('fill table with question numbers');
            });
        }
        else {
            $("#nextquestion").fadeOut("slow", function () {
                $("#singleqa").fadeIn(550, function () {
                    // Animation complete
                });
            });
            socket.emit("dividequiz screens");
        }
        //showloader();
        return false;
    });

    socket.on("initiate live leaderboard", function (users) {
        for (var i = 0; i < users.length; i++) {
            $("#liveleaderboard").append('<button class="mdl-chip leader-chip mdl-badge" data-badge="0" id="' + users[i].username + '">\n' +
                '                <span class="mdl-chip__contact" style=\'background-image: url("https://robohash.org/' + users[i].username + '?size=50x50&set=set3");\'>&nbsp;</span>\n' +
                '            <span class="mdl-chip__text leader-username">' + users[i].username + '</span>\n' +
                '            </button>');
        }

    });

    socket.on("show jump links to students", function (data) {
        $("#jumptolinks").html('<div class="mdl-sheet__container">\n' +
            '    <div class="mdl-sheet mdl-shadow--2dp jumptosheet">\n' +
            '        <i class="material-icons mdl-sheet__icon">redo</i>\n' +
            '\n' +
            '        <div class="mdl-sheet__content" id="jumptotext">\n' +
            '        <h5>Jump to any question from here:</h5><br/></div>\n' +
            '    </div>\n' +
            '</div>');
        for (var i = 0; i < data.all_questions.length; i++) {
            $("#jumptotext").append('<a class="jumpto" href="#" id="' + data.all_questions[i].id + '" jump-to="' + i + '">' + (i + 1) + ': ' + data.all_questions[i].question + '</a><br/><br/>');
        }
        var $jumpsheet = $('.jumptosheet');

        if ($jumpsheet.length > 0) {
            $('html').on('click', function () {
                $jumpsheet.removeClass('mdl-sheet--opened')
            });

            $jumpsheet.on('click', function (event) {
                event.stopPropagation();

                $jumpsheet.addClass('mdl-sheet--opened');
            });
        }

        $(".jumpto").click(function (event) {
            event.preventDefault();
            $("#nextquestion").fadeOut("slow", function () {
                $("#singleqa").fadeIn(550, function () {
                    // Animation complete
                });
            });
            // $(this).addClass('strike');
            $('.jumptosheet').removeClass('mdl-sheet--opened');
            //showloader();

            var jumpTo = parseInt($(this).attr('jump-to'));


            //console.log('jumping to ' + jumpTo);

            socket.emit("show question to students", {jump_to: jumpTo});
            socket.emit("show answers to students", {jump_to: jumpTo});
            //console.log($(this).attr('id'));
        });
    });

    socket.on("refresh live leaderboard", function (user) {
        $("#liveleaderboard").append('<button class="mdl-chip leader-chip mdl-badge" data-badge="0" id="' + user + '">\n' +
            '                <span class="mdl-chip__contact" style=\'background-image: url("https://robohash.org/' + user + '?size=50x50&set=set3");\'>&nbsp;</span>\n' +
            '            <span class="mdl-chip__text leader-username">' + user + '</span>\n' +
            '            </button>');
    });

    socket.on('add question numbers in table', function (data) {
        //console.log('total questions: ' + data.total_questions.length);
        for (var i = 0; i < data.total_questions.length; i++) {
            //console.log('here big one: ' + data.total_questions[i].id);
            ranks[i] = [];
            $("#rtstatscontent").append('<tr id="trquestion' + i + '">\n' +
                '      <td id="idquestion' + i + '">' + (i + 1) + '</td>\n' +
                '      <td id="correctquestion' + i + '">0</td>\n' +
                '      <td id="partialcorrectquestion' + i + '">0</td>\n' +
                '      <td id="incorrectquestion' + i + '">0</td>\n' +
                '      <td id="fastestquestion' + i + '" class="mdl-data-table__cell--non-numeric">-</td>\n' +
                '      <td id="toughestone' + i + '" class="mdl-data-table__cell--non-numeric">-</td>\n' +
                '</tr>\n');
        }
        total_qs = $('#rtstatstable tr[id^=\'trquestion\']').length;
        //console.log('Total questions: ' + total_qs);
        $("#resultsscreen").fadeIn("slow", function () {
            socket.emit('show question to students');
            socket.emit('show answers to students');
        });
    });

    socket.on('show next question to students', function (data) {
        //console.log('Next question show');
        if (data) {
            if (data.first_time == "yes") {
                $("#loginas").remove();
                $("#body").css('background-image', 'none');
                socket.emit('show question to students');
                socket.emit('show answers to students');
                //console.log('came to show next question to students in first time');
            }
        }
        else {
            if (!$('#loginas').length && !$('#waiting').length) {
                setTimeout(function () {
                    socket.emit('show question to students');
                    socket.emit('show answers to students');
                }, 1000);
                //console.log('came to show next question to students');
            }
        }
    });

    socket.on('quiz ended fixedtime', function () {
        if (!$('#loginas').length && !$('#waiting').length) {
            // $("input[id^='answer']").attr("disabled", true);
            //console.log('quiz ended in front');
            $("body").append('<div style="height: 100vh; width: 100vw; background: rgba(0,0,0,0.8); color: white;" id="quizendeddiv">' +
                '<div class="quizenddivcontent vertical-middle">' +
                '<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="endquiznow">Finish Quiz?</a>' +
                '<a style="margin-top: 40px;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="gobacktoquestions">Go through questions again ?</a>' +
                '</div>' +
                '</div>');
            $("#gobacktoquestions").click(function (event) {
                event.preventDefault();
                $("#quizendeddiv").remove();
            });
            $("#endquiznow").click(function (event) {
                event.preventDefault();
                $(".quizenddivcontent").empty();
                $("#jumptolinks").remove();
                $(".mdl-sheet__container").remove();
                $(".feedbacksopener").remove();
                $("#container").remove();
                $("#startConfetti").remove();
                $("#stopConfetti").remove();
                $("#stopConfetti").remove();
                $(".quizenddivcontent").html('<h1 style="text-align: center;">You have successfully finished the quiz. Please ask your lecturer for next step.</h1>');
            })
        }
    });

    socket.on('start timer', function (time) {
        if (!$('#loginas').length) {
            //console.log('Timer starts');
            $("#header").append('<div id="fixedtimerdiv" class="mdl-cell mdl-cell--6-col notopmar">Time left: <span id="fixedtimer"></span></div>');
            socket.emit('sync all timers', {time: time});
        }
    });

    socket.on('sync timer', function (data) {
        //console.log('Timer syncing');
        if (!$('#fixedtimerdiv').length && !$('#loginas').length) {
            $("#header").append('<div id="fixedtimerdiv" class="mdl-cell mdl-cell--6-col notopmar">Time left: <span id="fixedtimer"></span></div>');
        }

        function get_elapsed_time_string(total_seconds) {
            function pretty_time_string(num) {
                return (num < 10 ? "0" : "") + num;
            }

            var hours = Math.floor(total_seconds / 3600);
            total_seconds = total_seconds % 3600;

            var minutes = Math.floor(total_seconds / 60);
            total_seconds = total_seconds % 60;

            var seconds = Math.floor(total_seconds);

            // Pad the minutes and seconds with leading zeros, if required
            hours = pretty_time_string(hours);
            minutes = pretty_time_string(minutes);
            seconds = pretty_time_string(seconds);

            // Compose the string for display
            var currentTimeString = hours + ":" + minutes + ":" + seconds;

            return currentTimeString;
        }

        var elapsed_seconds = data.time;

        if (data.time != 0) {
            $('#fixedtimer').text(get_elapsed_time_string(elapsed_seconds));
        }
        else
            socket.emit('End Quiz Now');
    });

    socket.on('end the quiz for students', function () {
        $("body").append('<div style="height: 100vh; width: 100vw; background: rgba(0,0,0,0.8); color: white;" id="quizendeddiv">' +
            '<div class="quizenddivcontent vertical-middle">' +
            '<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="endquiznow">Finish Quiz?</a>' +
            '<a style="margin-top: 40px;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="gobacktoquestions">Go through questions again ?</a>' +
            '</div>' +
            '</div>');
        //console.log('end quiz for students');
        $(".quizenddivcontent").empty();
        $("#jumptolinks").remove();
        $(".mdl-sheet__container").remove();
        $(".feedbacksopener").remove();
        $("#container").remove();
        $("#startConfetti").remove();
        $("#stopConfetti").remove();
        $("#stopConfetti").remove();
        $(".quizenddivcontent").html('<h1 style="text-align: center;">Time\'s Up. Please ask your lecturer for next step.</h1>');
        $("#fixedtimerdiv").remove();
    });

    socket.on('end the quiz for teacher', function () {
        //console.log('end quiz for teacher');
        $("#fixedtimerdiv").html('<span style="text-align: center;">Time\'s Up. Please let participants know what to do next.</span>');
        $("#fixedtimer").remove();
    });

    socket.on('update stats', function (data) {

        //console.log('username: ' + data.user);

        //console.log('here is small one: ' + data.q_id);

        ranks[data.q_id].push(data.user);

        // for(var i=0; i<ranks[data.q_id].length; i++){
        //     //console.log('Ranks of: ' + i + ' : ' + ranks[data.q_id][i])
        // }

        var corrects = data.correct;
        var incorrects = data.incorrect;
        var partial = data.partial_correct;
        var score = data.score;


        // //console.log('Corrects: ' + corrects);
        // //console.log('incorrects: ' + incorrects);
        // //console.log('score: ' + score);
        // //console.log('partial: ' + partial);

        // //console.log('increased');
        // //console.log('this time score: ' + score);
        // //console.log('original value: ' + parseInt($("#"+ data.user).attr('data-badge')));
        $("#" + data.user).attr('data-badge', (parseInt($("#" + data.user).attr('data-badge')) + score - data.last_score));

        if (data.correct >= 0) {
            $("#correctquestion" + data.q_id).text(corrects);
        }
        if (data.partial_correct >= 0) {
            $("#partialcorrectquestion" + data.q_id).text(partial);
        }
        if (data.incorrect >= 0) {
            $("#incorrectquestion" + data.q_id).text(incorrects);
        }

        if (data.is_first) {
            $("#fastestquestion" + data.q_id).text(data.user);
        }

        if (data.user == $("#fastestquestion" + data.q_id).text() && score != 0) {
            find_first_user((last_rank + 1));
        }

        for (var i = 0; i < total_qs; i++) {
            if (data.toughestq == i) {
                $("#toughestone" + i).html('&#10003;');
            }
            else {
                $("#toughestone" + i).text('-');
            }
        }

        function find_first_user(index) {
            if (!ranks[data.q_id][index]) {
                $("#fastestquestion" + data.q_id).text('-');
            }
            else {
                last_rank = index;
                $("#fastestquestion" + data.q_id).text(ranks[data.q_id][index]);
            }
        }

        $("#trquestion" + data.q_id).fadeOut(500);
        $("#trquestion" + data.q_id).fadeIn(500);

        // shakeIt("trquestion" + data.q_id);
        //
        // function shakeIt(id) {
        //     //console.log('id is: ' + id);
        //     $(id).effect("shake",{times:2,distance:5,direction:"left"},5000,function(){
        //
        //     });
        //     $("#" + id).effect("shake");
        // };
    });


    // single question start screen for students
    socket.on('quiz screen for students', function (data) {
        if (!$('#loginas').length) {
            if (data) {
                //console.log('came to if data show q');
                if (data.goback == 1)
                    socket.emit('show q', {goback: 1});
            }
            else {
                //console.log('came to else data show q');
                socket.emit('show q');
            }
            setTimeout(function () {
                $(".questiontitle").append('<div id="timerdiv" class="mdl-cell mdl-cell--6-col notopmar"><span id="timer"></span></div>');
            }, 1000);
            $("#timerdiv").css('display', 'block');
            $("#timer").css('display', 'block');
            $("#timer").css("width", "90px");
            $("#timer").css("height", "90px");
            // setTimeout(function () {
            //
            // }, 6000);
            var n = 6;
            var tm = setInterval(countDown, 1000);

            function countDown() {
                n--;
                if (n == 0) {
                    clearInterval(tm);
                    $("#timerdiv").fadeOut("slow", function () {
                        socket.emit("show a");
                    });
                }
                $("#timer").text(n);
            }
        }
        else {
            // student hasn't logged in yet
        }
    });

    //single question start screen for teacher
    socket.on('quiz screen for teacher', function (data) {
        socket.emit('reset corrects and incorrects callback');
        socket.emit('reset choices selected callback');
        // $("#container").addClass('med-padding');
        if (data) {
            //console.log('came to if data show q');
            if (data.goback == 1)
                socket.emit('show q', {goback: 1});
        }
        else {
            //console.log('came to else data show q');
            socket.emit('show q');
        }
        setTimeout(function () {
            socket.emit("show a for teacher");
        }, 1000);
    });

    // show question
    socket.on('show question', function (data) {
        if (!$('#loginas').length) {
            $("#loginas").remove();

            if (data.type) {
                quiz_type = data.type;
            }
            $("#questionform > [id^='question']").remove();
            $("#resultsscreen").empty();
            $("#waiting").remove();
            $("#connectedusers").remove();
            $("#startquizscreen").remove();
            $("#user_new").remove();
            $("#user_left").remove();
            $("#nextquestion").fadeOut("slow", function () {
                $("#singleqa").fadeIn(550, function () {
                    // Animation complete
                });
            });
            //console.log(data['oldID']);
            $("#ques_form_grid" + data['oldID']).remove();
            // $('[id^=question]').remove();


            $("#button" + data['oldID']).remove();
            for (var i = 0; i < 4; i++) {
                $("#answer" + i + data['oldID']).remove();
                $("#label" + i + data['oldID']).remove();
            }

            $("#question" + data['oldID']).remove();
            $("#questionform").append('<div class="mdl-grid questiontitle question' + data['row'].id + '" id="question' + data['ques_id'] +
                '"></div>');
            $('#question' + data['ques_id']).append('<div class="mdl-cell mdl-cell--12-col"><h1>' + data['row'].question + '</h1></div>');
            if (data['row'].img_path) {
                image = true;
                $('#question' + data['ques_id']).append('<div class="mdl-cell mdl-cell--6-col notopmar"><img id="myImg" class="questionimage" alt="' + data['row'].img_path + '" src="http://vedaquiz.it/uploads/' + data['row'].img_path + '"/><div id="myModal" class="modal"><span class="close">&times;</span><img class="modal-content" id="img01"><div id="caption"></div></div></div>')
                var modal = document.getElementById('myModal');

                // Get the image and insert it inside the modal - use its "alt" text as a caption
                var img = document.getElementById('myImg');
                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");
                img.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = "http://vedaquiz.it/uploads/" + data['row'].img_path;
                    captionText.innerHTML = this.alt;
                }
                var span = document.getElementsByClassName("close")[0];
                span.onclick = function () {
                    modal.style.display = "none";
                }
            }
            else {
                image = false;
            }

            $("#startq").remove();
        }
    });

    // show answers to students
    socket.on('show answers', function (data) {
        teacher = false;
        var gridClassMain;
        var gridClassMainInside;
        var gridClassSubmit;

        if (image == true) {
            gridClassMain = 'mdl-cell mdl-cell--6-col notopmar';
            gridClassMainInside = 'mdl-cell mdl-cell--12-col';
            gridClassSubmit = 'mdl-cell mdl-cell--6-col martop';

        }
        else {
            gridClassMain = 'mdl-cell mdl-cell--12-col mdl-grid';
            gridClassMainInside = 'mdl-cell mdl-cell--6-col';
            gridClassSubmit = 'mdl-cell mdl-cell--4-col martop';
            // $("#questionform").addClass('vertical-middle');
        }

        q_id = data.ans[0].ques_id;
        //console.log(q_id);
        var previousAns = data.ans[0].ques_id - 1;
        $("#button" + previousAns).remove();
        for (var i = 0; i < 4; i++) {
            $("#answer" + i + previousAns).remove();
            $("#label" + i + previousAns).remove();
        }
        var multi = 0;
        var type_input = 'radio';
        for (var i = 0; i < data.ans.length; i++) {
            if (data.ans[i].is_true == 1) {
                multi++;
            }
        }

        $(".question" + data.ans[0].ques_id).append('<div class="' + gridClassMain + '" id="ques_form_grid' + data.ans[0].ques_id + '"></div>');
        if (multi >= 2) {
            type_input = 'checkbox';
        }
        if (type_input == 'checkbox') {
            for (var i = 0; i < data.ans.length; i++) {
                $("#ques_form_grid" + data.ans[0].ques_id).append('<div class="' + gridClassMainInside + '"><label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="answer' + i + data.ans[i].ques_id + '"><input type="' + type_input + '" value="' + data.ans[i].answer + '" class="mdl-checkbox__input" name="' + data.ans[i].answer + '" id="answer' + i + data.ans[i].ques_id + '"/>' + '<p class="mdl-checkbox__label autore nodis" id="label' + i + data.ans[i].ques_id + '"><span>' + data.ans[i].answer + '</span></p></label></div>');
            }
        }
        if (type_input == 'radio') {
            for (var i = 0; i < data.ans.length; i++) {
                if (data.ans[i].answer != '')
                    $("#ques_form_grid" + data.ans[0].ques_id).append('<div class="' + gridClassMainInside + '"><label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="answer' + i + data.ans[i].ques_id + '"><input type="' + type_input + '" value="' + data.ans[i].answer + '" class="mdl-radio__button"  name="answer" id="answer' + i + +data.ans[i].ques_id + '"/>' + '<p class="mdl-radio__label autore nodis" id="label' + i + data.ans[i].ques_id + '"><span>' + data.ans[i].answer + '</span></p></label></div>');
            }
        }
        $("#ques_form_grid" + data.ans[0].ques_id).append('<break></break><div class="' + gridClassSubmit + '"><span id="submitanswersstu"><input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" id="button' + data.ans[0].ques_id + '" value="Submit Answer"/></span></div>');
        // fade in answers one by one

        $('#label0' + data.ans[0].ques_id).fadeIn(300, function () {
            $('#label1' + data.ans[0].ques_id).fadeIn(300, function () {
                $('#label2' + data.ans[0].ques_id).fadeIn(300, function () {
                    $('#label3' + data.ans[0].ques_id).fadeIn(300, function () {
                        setTimeout(function () {
                            $('#submitanswersstu').fadeIn(300, function () {
                            });
                        }, 1000);
                    });
                });
            });
        });

        if (quiz_type == "FixedTime") {
            var last_A = data.A;
            var last_B = data.B;
            var last_C = data.C;
            var last_D = data.D;

            //console.log('prev A: ' + last_A);
            //console.log('prev B: ' + last_B);
            //console.log('prev C: ' + last_C);
            //console.log('prev D: ' + last_D);

            if (last_A == 'true') {
                $('#answer0' + data.ans[0].ques_id).prop('checked', true);
            }
            if (last_B == 'true') {
                $('#answer1' + data.ans[0].ques_id).prop('checked', true);
            }
            if (last_C == 'true') {
                $('#answer2' + data.ans[0].ques_id).prop('checked', true);
            }
            if (last_D == 'true') {
                $('#answer3' + data.ans[0].ques_id).prop('checked', true);
            }
        }

        $("#helptext").html('You can see current question and answers in this screen. If question has image, you can zoom in in image by clicking on it. Answers can be multiple correct or single correct unless told otherwise by lecturer. You can submit your answers, after selecting them, by clicking on "Submit Answer" button.');
        // $(function () {
        //     console.log('came to textfill function');
        //     $(".autore").each(
        //         function () {
        //             $(this).textfill({
        //                 minFontPixels: 10,
        //                 maxFontPixels: 20,
        //                 changeLineHeight: true,
        //                 // debug: true
        //             });
        //         })
        // });
    });

    //show answers as list to teacher
    socket.on('show answers to teacher', function (data) {
        $("#nextquestion").fadeOut("slow", function () {
            //console.log(image);
            teacher = true;
            var gridClassMain;
            var gridClassMainInside;
            var gridClassSubmit;
            if (image == true) {
                gridClassMain = 'mdl-cell mdl-cell--6-col notopmar';
                gridClassMainInside = 'mdl-cell mdl-cell--12-col';
                gridClassSubmit = 'mdl-cell mdl-cell--6-col martop';

            }
            else {
                gridClassMain = 'mdl-cell mdl-cell--12-col mdl-grid';
                gridClassMainInside = 'mdl-cell mdl-cell--6-col';
                gridClassSubmit = 'mdl-cell mdl-cell--4-col martop';
                $("#questionform").addClass('vertical-middle');
            }
            //console.log(".question" + data.currq);

            $(".question" + data.currq).append('<div class="' + gridClassMain + '" id="ques_form_grid_teacher"></div>');
            // console.log('number of answers are: ' + data.answers.length);
            // console.log(data.answers);
            for (var i = 0; i < data.answers.length; i++) {
                if (data.answers[i].answer != '')
                    $("#ques_form_grid_teacher").append('<div class="' + gridClassMainInside + '"><li class="autore' + i + ' nodis" value="' + data.answers[i].answer + '" name="' + data.answers[i].answer + '" id="answer' + i + data.answers[i].ques_id + '">' + '<span id="label' + i + data.answers[i].ques_id + '">' + data.answers[i].answer + '</span></li></div>');
            }

            $('#answer0' + data.answers[0].ques_id).fadeIn(300, function () {
                $('#answer1' + data.answers[1].ques_id).fadeIn(300, function () {
                    $('#answer2' + data.answers[2].ques_id).fadeIn(300, function () {
                        $('#answer3' + data.answers[3].ques_id).fadeIn(300, function () {
                            // Animation complete
                        });
                    });
                });
            });

            if (data.last_q == 1) {
                setTimeout(function () {
                    $("#ques_form_grid_teacher").append('<break></break><div class="' + gridClassSubmit + '"><button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent tsubq" id="tsubbut">End Question</button></div>');
                }, 9000);
                // button' + data.answers[0].ques_id + '
                next_q_avail = 1;
            }
            if (data.last_q == 0) {
                setTimeout(function () {
                    $("#ques_form_grid_teacher").append('<div class="' + gridClassSubmit + '"><button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent tsubq" id="button' + data.answers[0].ques_id + '">End Quiz</button></div>');
                }, 9000);
                next_q_avail = 0;
            }
            $("#helptext").html('You can see current question and answers in this screen. If question has image, you can zoom in in image by clicking on it. Answers for lecturers are NOT selectable. You can end the question by clicking on "End Question" or "End Quiz" button. "End Quiz" button appears when you are at last question.');
            // $(function () {
            //     $(".autore").each(
            //         function () {
            //             console.log($(this));
            //             $(this).textfill({
            //                 minFontPixels: 10,
            //                 maxFontPixels: 20,
            //                 changeLineHeight: true,
            //                 debug: true
            //             });
            //         })
            // });
        });

        // var n = 6;
        // var tm = setInterval(countDown, 1000);
        //
        // function countDown() {
        //     $("#timer").css("display", "block");
        //     n--;
        //     if (n == 0) {
        //         $("#timer").css("height", "0");
        //         $("#timer").css("width", "0");
        //     }
        // }


    });

    // send answers to server.js for validation
    $("#questionform").submit(function () {
        if (quiz_type == 'FixedTime') {
            socket.emit('check answer and show next question', {
                answer1: $('#answer0' + q_id).val(),
                ansstatus1: $('#answer0' + q_id).is(":checked"),
                answer2: $('#answer1' + q_id).val(),
                ansstatus2: $('#answer1' + q_id).is(":checked"),
                answer3: $('#answer2' + q_id).val(),
                ansstatus3: $('#answer2' + q_id).is(":checked"),
                answer4: $('#answer4' + q_id).val(),
                ansstatus4: $('#answer3' + q_id).is(":checked"),
                question_id: q_id
            });
            //console.log("Question id with strike #"+q_id);
            $("#" + q_id).addClass('strike');
            //showloader();
            return false;
        }
        else {
            if (teacher == false) {
                // console.log('proper ans sub event');
                socket.emit('answer submitted', {
                    answer1: $('#answer0' + q_id).val(),
                    ansstatus1: $('#answer0' + q_id).is(":checked"),
                    answer2: $('#answer1' + q_id).val(),
                    ansstatus2: $('#answer1' + q_id).is(":checked"),
                    answer3: $('#answer2' + q_id).val(),
                    ansstatus3: $('#answer2' + q_id).is(":checked"),
                    answer4: $('#answer4' + q_id).val(),
                    ansstatus4: $('#answer3' + q_id).is(":checked"),
                    question_id: q_id
                });
                //showloader();
                return false;
            }
            else {
                //console.log('came to teacher event');
                socket.emit('answer ended by teacher');
                // event.preventDefault();
                //showloader();
                return false;
            }
        }
    });

    //Question ended by teacher
    // $("#answerslist").submit(function () {
    //     socket.emit('answer ended by teacher');
    //     //showloader();
    //     return false;
    // });

    // $("#tsubbut").click(function (event) {
    //     //console.log('came to click event');
    //     socket.emit('answer ended by teacher');
    //     event.preventDefault();
    //     //showloader();
    //     // return false;
    // });

    // show next question
    // $("#nextquestion").submit(function () {
    //     $( "#resultsscreen" ).fadeOut( "slow", function() {
    //         $( "#startquizscreen" ).fadeOut( "slow", function() {
    //             $( "#singleqa" ).fadeIn( 550, function() {
    //                 // Animation complete
    //             });
    //         });
    //     });
    //     $(".chart-container").remove();
    //
    //     socket.emit('dividequiz screens');
    //     //showloader();
    //     return false;
    // });

    // show single result for specific user
    socket.on('singleresult', function (data) {
        //console.log('came in single result');
        if (!$('#loginas').length && !$('#waiting').length) {
            //console.log('went inside single result');
            var previousBoard = data.que_id - 1;
            $("#singleresult" + previousBoard).remove();
            $("#leaderboa" +
                "rd" + previousBoard).remove();
            $("#singleqa").fadeOut("slow", function () {
                $("#resultsscreen").css("display", "flex");
                $("#nextquestion").remove();
                // $("#resultsscreen").append("<div class=\"notice mdl-cell mdl-cell mdl-cell--12-col\" id='singleresult'></div>")
                $("#helptext").html('You will see your result once lecturer ends the question.');
                $("#nextquestion").fadeIn(550, function () {
                    // Animation complete
                });
                $("#submitanswersstu").remove();
                if (data.is_poll != 'true') {
                    for (var i = 0; i < 4; i++) {
                        $("#answer" + i + data.que_id).attr("disabled", true);
                        if (data.correct[i] == -1) {
                            $("#label" + i + data.que_id).addClass("wronganswer");
                        }
                        if (data.correct[i] == 1) {
                            $("#label" + i + data.que_id).addClass("correctanswer");
                        }
                        if (data.correct[i] == 2) {
                            $("#label" + i + data.que_id).addClass("missedanswer");
                        }
                    }
                }
            });
            $("#resultsscreen").append("<div class=\"mdl-cell mdl-cell--10-col v-center\" id='studentchoicedivsingle'></div>");
            // How many students chose which choice of answer
            //console.log('added chart container');
            $("#studentchoicedivsingle").append("<div class=\"chart-container\" style=\"position: relative; height:auto; width:100%; margin: 0 auto;\">\n" +
                "    <canvas id=\"studentChoicesSingle\"></canvas>\n" +
                "</div>\n");
            if (data.flag == 1) {
                $("#studentchoicedivsingle").prepend('<div class="mdl-cell mdl-cell mdl-cell--12-col" id=\'singleresult\'><h3>You will see result and score after the teacher ends the question.</h3></div>');
                correct_given++;
            }
            else {
                $("#studentchoicedivsingle").prepend('<div class="mdl-cell mdl-cell mdl-cell--12-col" id=\'singleresult\'><h3>You will see result and score after the teacher ends the question.</h3></div>');
                incorrect_given++;
            }
        }
    });

    socket.on('refresh student choices chart', function (data) {
        //console.log('came in refresh charts: studentChoicesSingle');
        var ctx = document.getElementById("studentChoicesSingle");
        if(ctx) {
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["A", "B", "C", "D"],
                    datasets: [{
                        label: 'Choices chosen by students',
                        data: [data.A, data.B, data.C, data.D],
                        backgroundColor: [
                            'rgba(255, 159, 64, 0.4)',
                            'rgba(255, 205, 86, 0.4)',
                            'rgba(75, 192, 192, 0.4)',
                            'rgba(54, 162, 235, 0.4)'
                        ],
                        borderColor: [
                            'rgb(255, 159, 64)',
                            'rgb(255, 205, 86)',
                            'rgb(75, 192, 192)',
                            'rgb(54, 162, 235)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function (value) {
                                    return Number(value).toFixed(0);
                                }
                            }
                        }]
                    }
                }
            });
        }
    });

    socket.on('make chart as modal now', function (data) {
        $("#studentchoicedivsingle").remove();
        $("#resultsscreen").css('display', 'none');
        //console.log('data.ques' + data.ques);
        $("#single" + (data.ques - 1)).remove();
        $("#question" + data.ques).append('<div class="mdl-cell mdl-cell--3-col"><a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent martopm" data-popup-open="singlestuchoices" href="#" id="">Check Choice Preferences</a></div>');
        $("#header").append('<div class="mdl-cell mdl-cell--12-col" id="single' + data.ques + '"><div class="popup" data-popup="singlestuchoices" id="singlestuchoices">\n' +
            '    <div class="popup-inner">\n' +
            //'        <h2 id="leader-title">Student Choices</h2>\n' +
            '        <div id=\'singlestuchoicesp' + data.ques + '\'><div class="chart-container" style="position: relative; height:auto; width:100%; margin: 0 auto;"><canvas id="studentChoicesSingle' + data.ques + '"></canvas></div></div>\n' +
            '        <div class="mdl-cell mdl-cell--4-col"><p><a data-popup-close="singlestuchoices" href="#" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Close</a></p></div>\n' +
            '        <a class="popup-close" data-popup-close="singlestuchoices" href="#">x</a>\n' +
            '    </div>\n' +
            '</div></div>');
        $(function () {
            //----- OPEN
            $('[data-popup-open]').on('click', function (e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

                e.preventDefault();
            });

            //----- CLOSE
            $('[data-popup-close]').on('click', function (e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

                e.preventDefault();
            });

            // show celebration confettis
            if (is_correct == true) {
                $("#startConfetti").trigger("click");
                setTimeout(function () {
                    $("#stopConfetti").trigger("click");
                }, 1500);
            }

        });
        var ctx = document.getElementById("studentChoicesSingle" + data.ques);
        if(ctx) {
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["A", "B", "C", "D"],
                    datasets: [{
                        label: 'Choices chosen by students',
                        data: [data.A, data.B, data.C, data.D],
                        backgroundColor: [
                            'rgba(255, 159, 64, 0.4)',
                            'rgba(255, 205, 86, 0.4)',
                            'rgba(75, 192, 192, 0.4)',
                            'rgba(54, 162, 235, 0.4)'
                        ],
                        borderColor: [
                            'rgb(255, 159, 64)',
                            'rgb(255, 205, 86)',
                            'rgb(75, 192, 192)',
                            'rgb(54, 162, 235)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    legend: {
                        labels: {
                            fontColor: 'white'
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                fontColor: 'white',
                                callback: function (value) {
                                    return Number(value).toFixed(0);
                                }
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                fontColor: 'white'
                            },
                        }]

                    }
                }
            });
        }
    });


    socket.on('stop displaying the result status to students', function (data) {
        $("#singleresult").empty();
    });

    // show next question button to teacher
    socket.on('show next question option', function (data) {
        var previousBoard = data.ques_id - 1;
        $("#singleresult" + previousBoard).remove();
        $("#singleqa").fadeOut("slow", function () {
            $("#nextquestion").fadeIn(550, function () {
                $("#singleqa").addClass('qapop');
                // Animation complete
                if (data.prev_q == 1) {
                    $("#res-left-col").append('<div class="mdl-cell mdl-cell--12-col topmar"><a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" href="#" id="prev_q">Restart Question</a></div>');
                }
                $("#prev_q").click(function (event) {
                    event.preventDefault();
                    $("#singleqa").removeClass('qapop');
                    $("#resultsscreen").fadeOut("slow", function () {
                        $("#startquizscreen").fadeOut("slow", function () {
                            $("#singleqa").fadeIn(550, function () {
                            });
                        });
                    });
                    $(".chart-container").remove();
                    socket.emit('dividequiz screens', {prev_q: 1});
                });
            });
        });

        if (next_q_avail == 0) {
            $(".nextq").css('display', 'none');
            $("#leader-title").text('Final Scores');
            socket.emit('submit feedbacks');
        }
        else {
            $(".nextq").css('display', 'block');
        }
        $("#helptext").html('You can see statistics of last question in this page. <br/>Diagram of last question is available if needed to explain answers. <br/>You can check the leaderboard by clicking on "Show Leader Board" or "Show Final Scores" button. <br/> First diagram shows total number of correct and wrong answers given for last question. Second diagram shows the choices preferred as answers in last question.');

    });


    // socket.on('hide single question div', function () {
    //     // $("#singleqa").css("display", "none");
    //     ////console.log('display hidden');
    // });

    socket.on('modify last leader board', function (data) {
        if (data.is_poll != 'true') {
            if (next_q_avail == 0) {
                $("#leader-title").text('Final Scores');
                $("#res-left-col").append('<div class="mdl-cell mdl-cell--12-col topmar"><a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" data-popup-open="popup-1" href="#" id="leader-button">Open Leader Board</a></div>');
                var $div = $('#leader-button').parent();
                $div.removeClass("mdl-cell--6-col")
                    .addClass("mdl-cell--8-col");
                $("#leader-button").text('Final Scores');
            }
            else {
                $("#leader-button").text('Leader Board');
                $("#res-left-col").append('<div class="mdl-cell mdl-cell--12-col topmar"><a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" data-popup-open="popup-1" href="#" id="leader-button">Open Leader Board</a></div>');
            }
            $(function () {
                //----- OPEN
                $('[data-popup-open]').on('click', function (e) {
                    var targeted_popup_class = jQuery(this).attr('data-popup-open');
                    $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

                    e.preventDefault();
                });

                //----- CLOSE
                $('[data-popup-close]').on('click', function (e) {
                    var targeted_popup_class = jQuery(this).attr('data-popup-close');
                    $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

                    e.preventDefault();
                });
            });
        }
    });


    //show leader board
    socket.on('leader board', function (results) {
        $("#header > [id^='l']").remove();
        if (!$('#loginas').length && !$('#waiting').length) {
            $('#l' + results.que_id).remove();
            //console.log('in leader board: ' + results.data[0].score);
            // $("#answerslist").remove();
            // $("#answerslist").addClass('qapop');
            $("#submitanswersstu").empty();
            $("#singleqa").fadeIn(550, function () {
                // Animation complete
            });
            var previousBoard = results.que_id - 1;
            $("#l" + previousBoard).remove();
            $("#resultsscreen").css("display", "flex");
            $("#header").prepend('\n' +
                '\n' +
                '<div class="mdl-cell mdl-cell--12-col" id="l' + results.que_id + '"><div class="popup" data-popup="popup-1">\n' +
                '    <div class="popup-inner">\n' +
                //  '        <h2 id="leader-title">Leader Board</h2>\n' +
                '        <p id=\'leaderboard' + results.que_id + '\'></p>\n' +
                '        <div style="margin: auto; width: 100px;"><a data-popup-close="popup-1" href="#" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Close</a></div>\n' +
                //'        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>\n' +
                '    </div>\n' +
                '</div></div>\n');
            // $("#leaderboard" + results.que_id).append('<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="leader_table">\n' +
            //     '  <thead>\n' +
            //     '    <tr>\n' +
            //     '      <th>Rank</th>' +
            //     '      <th class="mdl-data-table__cell--non-numeric">Username</th>\n' +
            //     '      <th>Score</th>\n' +
            //     '    </tr>\n' +
            //     '  </thead>' +
            //     '<tbody id="leader_tbody"></tbody></table>');


            for (var i = 0; i < results.data.length; i++) {
                //console.log(i + ' score: ' + results.data[i].score);
                // $("#leader_tbody").append('<tr><td>' + (i + 1) + '</td><td class="mdl-data-table__cell--non-numeric">' + results.data[i].username + '</td><td>' + results.data[i].score + '</td></tr>');
                $("#leaderboard" + results.que_id).append('<button class="mdl-chip leader-chip mdl-badge" data-badge="' + results.data[i].score + '">\n' +
                    '                <span class="mdl-chip__contact" style=\'background-image: url("https://robohash.org/' + results.data[i].username + '?size=50x50&set=set3");\'>&nbsp;</span>\n' +
                    '            <span class="mdl-chip__text leader-username">' + results.data[i].username + '</span>\n' +
                    '            </button>');
            }
            if ($('#timer').text().length == 0) {
                $("#resultsscreen").append('<div class="mdl-cell mdl-cell--6-col" id="res-left-col"></div>');
                if (results.img[0].img_path != '') {
                    $("#res-left-col").append('<div class="mdl-cell mdl-cell--12-col"><img style="width: auto; height: 100px;" id="myImgRes" src="http://vedaquiz.it/uploads/' + results.img[0].img_path + '" alt="' + results.img[0].img_path + '"></img><div id="myModalRes" class="modal"><span class="closeRes">&times;</span><img class="modal-content" id="img01Res"><div id="captionRes"></div></div></div>');
                    var modal = document.getElementById('myModalRes');

                    // Get the image and insert it inside the modal - use its "alt" text as a caption
                    var img = document.getElementById('myImgRes');
                    var modalImg = document.getElementById("img01Res");
                    var captionText = document.getElementById("captionRes");
                    img.onclick = function () {
                        modal.style.display = "block";
                        modalImg.src = "http://vedaquiz.it/uploads/" + results.img[0].img_path;
                        captionText.innerHTML = this.alt;
                    }
                    var span = document.getElementsByClassName("closeRes")[0];
                    span.onclick = function () {
                        modal.style.display = "none";
                    }

                    // $("#nextquestion").submit(function () {
                    //     $( "#resultsscreen" ).fadeOut( "slow", function() {
                    //         $( "#startquizscreen" ).fadeOut( "slow", function() {
                    //             $( "#singleqa" ).fadeIn( 550, function() {
                    //                 // Animation complete
                    //             });
                    //         });
                    //     });
                    //     $(".chart-container").remove();
                    //
                    //     socket.emit('dividequiz screens');
                    //     //showloader();
                    //     return false;
                    // });
                }
                if (results.is_poll != 'true')
                    $("#res-left-col").append('<div class="mdl-cell mdl-cell--12-col topmar"><a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent qa" href="#">Correct Answers</a></div>');
                else
                    $("#res-left-col").append('<div class="mdl-cell mdl-cell--12-col topmar"><a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent qa" href="#">Show Question and Answers</a></div>');
                setTimeout(function () {
                    if (next_q_avail == 1) {
                        $("#res-left-col").append('<div class="mdl-cell mdl-cell--12-col topmar"><a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent nextq" href="#">Start Next Question</a></div>');
                    }
                    else {
                        $(".notice").empty();
                        $("#quiz_ended").css('display', 'none');
                        $("#quiz_ended").html('<span class="noticespan">All questions have been answered. Thank you for your participation.</span>');
                        $("#quiz_ended").slideDown("slow", function () {
                            $("#startConfetti").trigger("click");
                            setTimeout(function () {
                                $("#stopConfetti").trigger("click");
                            }, 1500);
                        });
                    }
                    $('.nextq').click(function (event) {
                        $("#singleqa").removeClass('qapop');
                        $("#resultsscreen").fadeOut("slow", function () {
                            $("#startquizscreen").fadeOut("slow", function () {
                                $("#singleqa").fadeIn(550, function () {
                                });
                            });
                        });
                        $(".chart-container").remove();

                        socket.emit('dividequiz screens');
                        event.preventDefault();
                    });
                }, 1000);
                $(".tsubq").remove();
                $("#ques_form_grid_teacher").append('<div class="mdl-cell mdl-cell--6-col topmar"><a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent qaclose" href="#">See Results</a>')
                $(function () {
                    $('.qa').click(function (event) {

                        $(".qapop").fadeIn(550, function () {
                            // Animation complete
                        });
                        event.preventDefault();
                    });
                    $('.qaclose').click(function (event) {
                        $(".qapop").fadeOut(550, function () {

                        });
                        event.preventDefault();
                    });
                });
            }
            else {
                for (var i = 0; i < 4; i++) {
                    $("#answer" + i + results.last_q_id).attr("disabled", true);
                    ////console.log('results q id: ' + results.last_q_id);
                }
                $("#helptext").html('You can see result of last question in this page. Diagram of question is still available. <br/> Correct answers are blinking in your screen. <br/><br/> Your answers are divided in color code to indicate below: <br/><b>Green:</b> Correct Choice<br/><b>Red:</b> Wrong Choice<br/><b>Light Blue:</b> Missed Correct Answer<br/>');
                $("#question" + results.que_id).append('<div class="mdl-cell mdl-cell--12-col topmar botmar queend"><h5>Question Ended By Teacher. You can not make any changes now.</h5></div><br/>');
                if (results.is_poll != 'true') {
                    $("#question" + results.que_id).append('<div class="mdl-cell mdl-cell--3-col"><a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" data-popup-open="popup-1" href="#" id="leader-button">Open Leader Board</a></div>');
                    $(function () {
                        //----- OPEN
                        $('[data-popup-open]').on('click', function (e) {
                            var targeted_popup_class = jQuery(this).attr('data-popup-open');
                            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

                            e.preventDefault();
                        });

                        //----- CLOSE
                        $('[data-popup-close]').on('click', function (e) {
                            var targeted_popup_class = jQuery(this).attr('data-popup-close');
                            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

                            e.preventDefault();
                        });
                    });
                }
            }

            // to display correct answers everywhere
            if (results.is_poll != 'true') {
                for (var i = 0; i < 4; i++) {

                    if (results['total_answers'][i].is_true == 1) {
                        if ($("#label" + i + results.last_q_id).hasClass("wronganswer") || $("#label" + i + results.last_q_id).hasClass("missedanswer") || $("#label" + i + results.last_q_id).hasClass("correctanswer")) {
                            $("#label" + i + results.last_q_id).addClass("pulse");
                        }
                        else {
                            $("#label" + i + results.last_q_id).addClass("missedanswer pulse");
                        }
                    }
                    else {
                    }
                }
            }
        }
    });

    socket.on('check if last user left', function () {
        socket.emit('Check for last user in server');
    })

    socket.on('no students left in the quiz', function () {
        $(".notice").empty();
        $("#error_no_student_left").css('display', 'none');
        $("#error_no_student_left").html('<span class="noticespan">No students are left in quiz. Please close the page.</span>');
        $("#error_no_student_left").slideDown("slow", function () {
            // Animation complete.
        });

        $("#startquizscreentitle").empty();
        $("#startq").empty();
    })

    socket.on('no teachers left in the quiz', function () {
        $(".auxbar").fadeOut("slow", function () {
            $("#error_no_teacher_left").html('<p>Teacher left the quiz. Please close the page and join again after teacher.</p>');
        });
    })

    socket.on('please wait for teacher to login', function () {
        $(".notice").empty();
        $("#wait_for_teacher").css('display', 'none');
        $("#wait_for_teacher").html('<span class="noticespan">Please wait for teacher to login first and then try to login.</span>');
        $("#wait_for_teacher").slideDown("slow", function () {
            // Animation complete.
        });
    })

    socket.on('please enter proper username', function () {
        $(".notice").empty();
        $("#empty_name").css('display', 'none');
        $("#empty_name").html('<span class="noticespan">Name can not be empty.</span>');
        $("#empty_name").slideDown("slow", function () {
            // Animation complete.
        });
    })


    socket.on('teacher is already logged in', function () {
        $(".notice").empty();
        $("#teacher_logged_in").css('display', 'none');
        $("#teacher_logged_in").html('<span class="noticespan">One teacher is already logged in. You can only join as student. Please refresh page to get option.</span>');
        $("#teacher_logged_in").slideDown("slow", function () {
            // Animation complete.
        });
    })


    socket.on('error in password verification', function () {
        $(".notice").empty();
        $("#username_pass_no_match").css('display', 'none');
        $("#username_pass_no_match").html('<span class="noticespan">Username and password do not match. Please try again.</span>');
        $("#username_pass_no_match").slideDown("slow", function () {
            // Animation complete.
        });
    })

    socket.on('please select different username', function () {
        $(".notice").empty();
        $("#username_taken").css('display', 'none');
        $("#username_taken").html('<span class="noticespan">This username is already taken. Please enter different username to join the quiz.</span>');
        $("#username_taken").slideDown("slow", function () {
            // Animation complete.
        });
    })

    socket.on('refresh the currently joined students list', function (data) {
        $("#user_new").empty();
        $("#user_new").append('<h2><span class="connectedtitle">Currently Connected</span></h2>\n');
        for (var i = 0; i < data.length; i++) {
            // $("#user_new").append('<span class="mdl-chip connectedstu" id="' + data[i].username + 'connected">\n' +
            //     '    <span class="mdl-chip__text">' + data[i].username + '</span>\n' +
            //     '</span>');
            $("#user_new").append('<button class="mdl-chip leader-chip connectedstu" id="' + data[i].username + '">\n' +
                '                <span class="mdl-chip__contact" style=\'background-image: url("https://robohash.org/' + data[i].username + '?size=50x50&set=set3");\'>&nbsp;</span>\n' +
                '            <span class="mdl-chip__text leader-username">' + data[i].username + '</span>\n' +
                '            </button>');
        }
    });

    function showLoader() {
        $("#spinnerloader").css("display", "inline-block");
        $("#container").css("display", "none");
        setTimeout(function () {
            $("#spinnerloader").fadeOut("slow", function () {
                // Animation complete
            });
            $("#container").css("display", "block");

        }, 1000);
        return;
    }

    socket.on('save session now', function () {
        socket.emit('save values in local machine');
    });

    socket.on('show submit feedback modal', function () {
        //console.log('came to show submit feedback modal');
        $("#feedback").fadeIn(550, function () {
            // Animation complete
        });
    });

    socket.on('feedback successful', function () {
        //console.log('came to show submit feedback modal');
        $("#feedbacktitle").html('Feedback successfully submitted. Thank you.');
        $("#feedbacktitle").css('font-size', '72px');
        $("#feedbacktitle").css('margin-bottom', '40px');
        $("#feedbacksubtitle").html('Form will close automatically in 5 seconds. If not, please click on X below');
        $("#feedbacksubtitle").css('margin-bottom', '40px');
        $(".closefeed").html('X');
        $(".closefeed").css('font-size', '32px');
        $("#feedback_students").fadeOut("slow", function () {
            $("#feedback").fadeIn(550, function () {
                // Animation complete
            });
        });
        setTimeout(function () {
            $("#feedback").fadeOut("slow", function () {
                // Animation complete
            });
        }, 6000);
    });

    socket.on('answer submitted by student snackbar', function (data) {
        (function () {
            'use strict';
            // var snackbarContainer = document.querySelector('#snackbar');
            // var showSnackbarButton = document.querySelector('#show-snackbar');
            // var handler = function (event) {
            //     showSnackbarButton.style.backgroundColor = '';
            // };
            var notification = document.querySelector('.mdl-js-snackbar');
            notification.MaterialSnackbar.showSnackbar(
                {
                    message: data['username'] + ' Submitted Answer :)'
                }
            );
        }());

    });


    // socket.on('reset corrects and incorrects', function () {
    //     socket.emit('reset corrects and incorrects callback');
    // });
    //
    // socket.on('reset choices selected', function () {
    //     socket.emit('reset choices selected callback');
    // });

    socket.on('show result chart', function (data) {
        $("#resultsscreen").append('<div class="mdl-cell mdl-cell--6-col" id="res-right-col">');
        if (data.is_poll != 'true') {
            $("#res-right-col").append("<div class=\"mdl-cell mdl-cell--12-col\"><div class=\"chart-container\" style=\"position: relative; height:auto; width:100%; margin: 0 auto;\">\n" +
                "    <canvas id=\"myChart\"></canvas>\n" +
                "</div></div>\n");
            var ctx = document.getElementById("myChart");
            if(ctx) {
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ["Right", "Wrong"],
                        datasets: [{
                            label: '# Of correct or incorrect answers given',
                            data: [data.corrects, data.incorrects],
                            // data: [10, 6],
                            backgroundColor: [
                                'rgba(75, 192, 192, 0.4)',
                                'rgba(255, 99, 132, 0.4)'
                            ],
                            borderColor: [
                                'rgba(75, 192, 192, 1)',
                                'rgba(255,99,132,1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) {
                                        return Number(value).toFixed(0);
                                    }
                                }
                            }]
                        }
                    }
                });
            }
        }

        $("#res-right-col").append("<div class=\"mdl-cell mdl-cell--12-col\"><div class=\"chart-container\" style=\"position: relative; height:auto; width:100%; margin: 0 auto;\">\n" +
            "    <canvas id=\"choicesUsed\"></canvas>\n" +
            "</div></div>\n");
        var chtx = document.getElementById("choicesUsed");
        ////console.log(data.corrects + " " + data.incorrects);
        if(chtx) {
            var choicesUsed = new Chart(chtx, {
                type: 'bar',
                data: {
                    labels: ["A", "B", "C", "D"],
                    datasets: [{
                        label: 'Choices selected by all students',
                        data: [data.AGiven, data.BGiven, data.CGiven, data.DGiven],
                        // data: [9, 12, 16, 3],
                        backgroundColor: [
                            'rgba(255, 159, 64, 0.4)',
                            'rgba(255, 205, 86, 0.4)',
                            'rgba(75, 192, 192, 0.4)',
                            'rgba(54, 162, 235, 0.4)'
                        ],
                        borderColor: [
                            'rgb(255, 159, 64)',
                            'rgb(255, 205, 86)',
                            'rgb(75, 192, 192)',
                            'rgb(54, 162, 235)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function (value) {
                                    return Number(value).toFixed(0);
                                }
                            }
                        }]
                    }
                }
            });
        }
    });

    socket.on('empty all and give error', function () {
        $("#body").remove();
        setTimeout(function () {
            alert('Provided URL is incorrect. Please check your URL and try again.');
        }, 1);
        var n = 1;
        var tm = setInterval(countDown, 1);

        function countDown() {
        }

        return;
    });
    socket.on('quiz is empty', function () {
        $("#body").remove();
        setTimeout(function () {
            alert('Quiz is empty. Please add questions to start quiz.');
        }, 1);
        var n = 1;
        var tm = setInterval(countDown, 1);

        function countDown() {
        }

        return;
    });

    socket.on('set correct var', function (data) {
        if (data.correct == true) {
            is_correct = true;
        }
        else {
            is_correct = false;
        }
    });


    window.addEventListener('beforeunload', onbeforeunload);

    function onbeforeunload(e) {
        //console.log('>>>> onbeforeunload called');
        e.returnValue = "false";
    };

    //feedback submit
    $("#feedback_students").submit(function () {
        socket.emit("feedback submitted", {
            feedback: $("#feed").val(),
            name: $("#name").val(),
            email: $("#email").val()
        });
        return false;
    });

    $('.closefeed').click(function (event) {
        event.preventDefault();
        $("#feedback").fadeOut("slow", function () {
            // Animation complete
        });
    });

    $('.feedbacksopener').click(function (event) {
        $("#feedback").fadeIn(550, function () {
            // Animation complete
        });
        event.preventDefault();
        $("#feedbacktitle").html('Please help us make this system better');
        $("#feedbacktitle").css('font-size', '2.5rem');
        $("#feedbacktitle").css('margin-bottom', '0.5rem');
        $("#feedbacksubtitle").html('Good or bad, let us know what you think about this system.');
        $("#feedbacksubtitle").css('margin-bottom', '0.5rem');
        $(".closefeed").html("I don't want to help");
        $(".closefeed").css('font-size', '14px');
        $("#feedback_students").css('display', 'block');
    });

    //some form styling

    $(".fadeonhover").hover(
        function () {
            $(this).animate({
                fontSize: "56px"
            }, 200);
            $(this).fadeTo(300, 1, function () {
            });
        }, function () {
            $(this).animate({
                fontSize: "52px"
            }, 200);
            $(this).fadeTo(300, 0.5, function () {
            });
        }
    );

});