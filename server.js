//core variables

var static = require('node-static');
var app = require("express")();
express = require('express');
var http = require('http').Server(app);
var io = require("socket.io")(http);
var nsp = io.of('/');
var pool = require('./config/DatabaseSettings').pool();
var LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./scratch');


// page variables
var connectCounter = 1;
var flag = 0;
var firstConnect = 1;
var roomCount = [];
// roomCount.fill(0);

//Global variables

global.ns = '';
global.ques_id = []; //ID of sequence .. i.e First question, second question ....
global.quiz_id = [];
global.current_question_id = []; //ID of the question which is currently selected
global.room_Occupied = [];
global.corrects_given = [];
global.incorrects_given = [];
global.partial_corrects_given = [];
global.givenAnsA = [];
global.givenAnsB = [];
global.givenAnsC = [];
global.givenAnsD = [];
global.quiz_questions = [];
global.is_traverse = 0;
global.curr_quiz_type = '';
global.teacher_socket = '';
global.quiz_started = [];
global.timer = [];
global.quiz_name = [];
global.is_poll = '';

// const emitter = new EventEmitter();
nsp.setMaxListeners(20);

//compression

var compression = require('compression');
app.use(compression());

// end compression

// for mailing from contact form of home page

var fs = require('fs');
var logger = require("morgan");
var nodemailer = require('nodemailer');
var mg = require('nodemailer-mailgun-transport');
var bodyParser = require('body-parser');
var nconf = require('nconf');
var auth = require('./config.json');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'});
// app.use(logger('dev'));
// app.use(logger('combined', { stream: accessLogStream }));


app.post("/contact", function (req, res) {
    var name = req.body.name;
    var email = req.body.email;
    var message = req.body.message;

    console.log('\nCONTACT FORM DATA: ' + name);

    // create transporter object capable of sending email using the default SMTP transport
    var transporter = nodemailer.createTransport(mg(auth));

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: email, // sender address
        to: 'vedaquiz@gmail.com', // list of receivers
        subject: 'Message from VedaQuiz Website Contact page', // Subject line
        text: 'Name: ' + name + '\n\n\nMessage: ' + message,
        err: false
    };
    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log('\nERROR: ' + error + '\n');
            //   res.json({ yo: 'error' });
        } else {
            console.log('\nRESPONSE SENT: ' + info.response + '\n');
            pool.getConnection(function (err, connection) {
                if (err) {
                    //console.log(err);
                    return;
                }
                //console.log('came inside connect for room: ' + ns);
                connection.query("INSERT INTO contact_form (name, email, message) VALUES ('" + name + "', '" + email + "','" + message + "')", function (err, datastored) {
                    if (err) throw err;
                    console.log("INSERT INTO contact_form (name, email, message) VALUES ('" + name + "', '" + email + "','" + message + "')");
                });
            });
        }
    });

    var mailOptions = {
        from: 'contact@vedaquiz.com', // sender address
        to: email, // list of receivers
        subject: 'Your message to VedaQuiz', // Subject line
        text: 'Name: ' + name + '\n\n\nMessage: ' + message + '\n\n\nWe will get back to you soon. \n\n\n Thank you,\nAdmin Team',
        err: false
    };
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log('\nERROR: ' + error + '\n');
            //   res.json({ yo: 'error' });
        } else {
            console.log('\nSent to user');
            res.redirect('http://vedaquiz.com/?message=success#contactform');
        }
    });
});


app.post("/joinquiz", function (req, res) {
    var quizid = req.body.quizid;

    console.log('\nQuiz ID: ' + quizid);
    res.redirect('http://qveda.co/' + quizid);

});

// end mailing from contact form from home page


//mail from signup form (variables are already defined so no need to do that again here

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'});
// app.use(logger('dev'));
// app.use(logger('combined', { stream: accessLogStream }));


app.post("/signup", function (req, res) {
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var email = req.body.email;
    var country = req.body.country;
    var phone = req.body.phone;
    var city = req.body.city;
    var institute = req.body.institute;
    var role = req.body.role;
    var nos = req.body.nos;
    var noq = req.body.noq;
    var nostudents = req.body.nostudents;

    // console.log('\nCONTACT FORM DATA: '+ name);
    var transporter = nodemailer.createTransport(mg(auth));

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: email, // sender address
        to: 'vedaquiz@gmail.com', // list of receivers
        subject: 'Sign Up Request For VedaQuiz', // Subject line
        text: 'Personal Details\n----------------\n\nFirst Name: ' + first_name + '\n\n\nLast Name: ' + last_name + '\n\n\nEmail Address: ' + email + '\n\n\nPhone Number: ' + phone + '\n\n\nCountry: ' + country + '\n\n\nCity: ' + city + '\n\n\nProfessional Details\n----------------\n\nInstitute: ' + institute + '\n\n\nRole: ' + role + '\n\n\nNumber of subjects: ' + nos + '\n\n\nNumber of Quiz: ' + noq + '\n\n\nNumber of expected students: ' + nostudents,
        err: false
    };
    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log('\nERROR: ' + error + '\n');
            //   res.json({ yo: 'error' });
        } else {
            console.log('\nRESPONSE SENT: ' + info.response + '\n');
            pool.getConnection(function (err, connection) {
                if (err) {
                    //console.log(err);
                    return;
                }
                //console.log('came inside connect for room: ' + ns);
                connection.query("INSERT INTO signup_requests (first_name, last_name, email, country, phone, city, institute, role, number_of_subjects, number_of_quizzes, number_of_students) VALUES ('" + first_name + "', '" + last_name + "','" + email + "', '" + country + "', '" + phone + "', '" + city + "', '" + institute + "', '" + role + "', '" + nos + "', '" + noq + "', '" + nostudents + "')", function (err, datastored) {
                    if (err) throw err;
                    console.log("INSERT INTO signup_requests (first_name, last_name, email, country, phone, city, institute, role, number_of_subjects, number_of_quizzes, number_of_students) VALUES ('" + first_name + "', '" + last_name + "','" + email + "', '" + country + "', '" + phone + "', '" + city + "', '" + institute + "', '" + role + "', '" + nos + "', '" + noq + "', '" + nostudents + "')");
                    res.redirect('http://vedaquiz.com/signup?message=success');
                });
            });
        }
    });
    var mailOptions = {
        from: 'signup@vedaquiz.com', // sender address
        to: email, // list of receivers
        subject: 'Your sign up application for VedaQuiz', // Subject line
        text: 'Details\n----------------\n\nFirst Name: ' + first_name + '\n\n\nLast Name: ' + last_name + '\n\n\nEmail Address: ' + email + '\n\n\nPhone Number: ' + phone + '\n\n\nCountry: ' + country + '\n\n\nCity: ' + city + '\n\n\nInstitute: ' + institute + '\n\n\nRole: ' + role + '\n\n\nNumber of subjects: ' + nos + '\n\n\nNumber of Quiz: ' + noq + '\n\n\nNumber of expected students: ' + nostudents + '\n\n\n Your application is currently in review. We will get back to you with results soon.\n\n\n Thank you,\nAdmin Team',
        err: false
    };
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log('\nERROR: ' + error + '\n');
            //   res.json({ yo: 'error' });
        } else {
            console.log('\nSent to user');
            res.redirect('http://vedaquiz.com/?message=success#signupform');
        }
    });
});

//end mail from signup form


nsp.on('connection', function (socket) {
    socket.on('pong', function () {
        // console.log("Pong received from client");
    });
    setTimeout(sendHeartbeat, 25000);

    function sendHeartbeat() {
        setTimeout(sendHeartbeat, 25000);
        nsp.emit('ping', {beat: 1});
    }
});


nsp.on('connection', function (socket) {
    nsp.emit('ping', {beat: 1});
    var nsBack = ns;
    ns = getUrlVars()["ns"];

    function getUrlVars() {
        var vars = {};
        var parts = socket.handshake.headers.referer.replace(/[?&]+([^=&]+)=([^&]*)/gi,
            function (m, key, value) {
                vars[key] = value;
            });
        return vars;
    }

    if (ns) {
        pool.getConnection(function (err, connection) {
            if (err) {
                //console.log(err);
                return;
            }
            connection.query("SELECT quiz_name,id,quiz_type,is_empty,is_poll FROM quiz WHERE roomname = '" + ns + "'", function (err, quizexist) {
                if (err) throw err;
                console.log(quizexist);
                // console.log('is empty here : ' + parseInt(quizexist[0].is_empty));


                console.log('came inside connect 2 for room: ' + ns);
                if (quizexist.length > 0) {
                    if (!parseInt(quizexist[0].is_empty) == 1) {
                        console.log()
                        is_poll = quizexist[0].is_poll;
                        console.log('poll is: ' + is_poll);
                        if (quizexist[0].quiz_type == 'Traverse') {
                            is_traverse = 1;
                            if (!roomCount[ns]) {
                                roomCount[ns] = 1;
                            }
                            quiz_name[ns] = quizexist[0].quiz_name;
                            ques_id[ns] = 0;
                            corrects_given[ns] = 0;
                            incorrects_given[ns] = 0;
                            quiz_id[ns] = quizexist[0].id;
                            socket.join(ns);
                            room_Occupied[socket.id] = ns;
                            connectCounter++;
                            socket.emit('callback for required files');
                        }
                        if (quizexist[0].quiz_type == 'Normal' || quizexist[0].quiz_type == 'realtime') {
                            if (!roomCount[ns]) {
                                roomCount[ns] = 1;
                            }
                            quiz_name[ns] = quizexist[0].quiz_name;
                            quiz_id[ns] = quizexist[0].id;
                            console.log('This is quiz id: ' + quiz_id[ns]);
                            console.log("QUIZ NAME IS : " + quizexist[0].quiz_name);
                            socket.join(ns);
                            room_Occupied[socket.id] = ns;
                            connectCounter++;
                            socket.emit('callback for required files');
                        }
                        if (quizexist[0].quiz_type == 'FixedTime') {
                            console.log('FixedTime quiz');
                            curr_quiz_type = 'FixedTime';
                            quiz_name[ns] = quizexist[0].quiz_name;
                            if (!roomCount[ns]) {
                                roomCount[ns] = 1;
                            }
                            if (localStorage) {
                                localStorage.setItem('ques_id' + socket.id, 0);
                                console.log(localStorage.getItem('ques_id' + socket.id));
                            }
                            else {
                                console.log('local storage is not supported');
                            }
                            // ques_id[ns] = 0;
                            quiz_id[ns] = quizexist[0].id;
                            socket.join(ns);
                            room_Occupied[socket.id] = ns;
                            connectCounter++;
                            socket.emit('callback for required files');
                        }
                        // else{
                        //     console.log('Quiz type not supported');
                        //     return 0;
                        // }
                    }
                    else {
                        socket.emit('quiz is empty');
                    }
                }
                else {
                    socket.emit('empty all and give error');
                }
            });
        });
    }
});

nsp.on('connection', function (socket) {
    socket.on('add all required files', function () {
        if (roomCount[ns] == 1) {
            pool.getConnection(function (err, connection) {
                if (err) {
                    console.log(err);
                    return;
                }
                console.log('req files');
                givenAnsA[ns] = 0;
                givenAnsB[ns] = 0;
                givenAnsC[ns] = 0;
                givenAnsD[ns] = 0;
                connection.query("DELETE FROM users WHERE room = '" + ns + "'", function (err, rows) {
                    if (err) throw err;
                });
                if (curr_quiz_type != 'FixedTime') {
                    ques_id[ns] = 0;
                    corrects_given[ns] = 0;
                    incorrects_given[ns] = 0;
                }
                if (is_traverse == 1 || curr_quiz_type == 'FixedTime') {
                    corrects_given[ns] = [];
                    incorrects_given[ns] = [];
                    partial_corrects_given[ns] = [];
                    connection.query("SELECT id, question, img_path FROM quiz_questions WHERE quiz_id = '" + quiz_id[ns] + "'", function (err, allquestions) {
                        if (err) throw err;
                        quiz_questions[ns] = allquestions;
                        quiz_started[ns] = 0;
                        if (curr_quiz_type == 'FixedTime') {
                            // console.log('came to fixedtime stuff');
                        }
                    });
                }
            });
            roomCount[ns]++;
        }
    });
});

require('./Logic_Modules/LoginForTeacher.js')(nsp);
require('./Logic_Modules/ConnectedUsersAndStartScreen.js')(nsp);
require('./Logic_Modules/AddStudent.js')(nsp);
require('./Logic_Modules/WaitForOtherStudents.js')(nsp);
require('./Logic_Modules/AnswerSubmittedByStudent.js')(nsp);
require('./Logic_Modules/AnswerEndedByTeacher.js')(nsp);
require('./Logic_Modules/SeparateQuizScreens.js')(nsp);
require('./Logic_Modules/ShowQuestionAndAnswerToStudent.js')(nsp);
require('./Logic_Modules/ShowAnswerToTeacher.js')(nsp);
require('./Logic_Modules/LastUserLeft.js')(nsp);
require('./Logic_Modules/Feedbacks.js')(nsp);
require('./Logic_Modules/FixedTimeQuiz/InitiateStatsTable.js')(nsp);
require('./Logic_Modules/FixedTimeQuiz/ShowQuestionToStudents.js')(nsp);
require('./Logic_Modules/FixedTimeQuiz/CheckAnswersAndNextQuestion.js')(nsp);
require('./Logic_Modules/FixedTimeQuiz/EndQuiz.js')(nsp);
require('./Logic_Modules/FixedTimeQuiz/SyncTimers.js')(nsp);


// Express routing

app.get("/", function (req, res) {
    if (req.query.ns) {
        res.sendFile(__dirname + '/index.html');
    }
    else {
        res.sendFile(__dirname + '/home.html');
    }
});
app.get("/home", function (req, res) {
    res.sendFile(__dirname + '/home.html');
});
app.get("/signup", function (req, res) {
    res.sendFile(__dirname + '/signup.html');
});
app.get("/privacy-policy", function (req, res) {
    res.sendFile(__dirname + '/privacy-policy.html');
});
app.get("/joinquiz", function (req, res) {
    res.sendFile(__dirname + '/joinquiz.html');
});
app.get("/sitemap", function (req, res) {
    res.sendFile(__dirname + '/sitemap.txt');
});

// End Express routing

http.listen(process.env.PORT || 80, function () {
    // console.log("List/ening on process.env.PORT || 5000");
});

// serve files from assets folder

// app.use('/assets', express.static('assets'))
//
// var file = new static.Server('./assets');
// require('http').createServer(function (request, response) {
//     request.addListener('end', function () {
//         //
//         // Serve files!
//         //
//         file.serve(request, response);
//     }).resume();
// }).listen(8080);

app.use('/dist', express.static('dist'))

var file = new static.Server('./dist');
require('http').createServer(function (request, response) {
    request.addListener('end', function () {
        //
        // Serve files!
        //
        file.serve(request, response);
    }).resume();
}).listen(8080);

// end file server
