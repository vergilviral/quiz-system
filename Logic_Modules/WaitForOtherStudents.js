module.exports = function(nsp) {
    nsp.on('connection', function (socket) {
        socket.on('waitingnow others', function () {
            socket.emit('waitingnow');
        });
    });
}