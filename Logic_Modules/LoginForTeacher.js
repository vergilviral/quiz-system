var pool = require('../config/DatabaseSettings').pool();

module.exports = function(nsp) {
    nsp.on('connection', function (socket) {
        socket.on('verify password', function (data) {
            // console.log('login' + corrects_given[ns][5]);
            var bcrypt = require('bcrypt-nodejs');
            var finalNodeGeneratedHash;
            var LocalStorage = require('node-localstorage').LocalStorage;
            localStorage = new LocalStorage('./scratch');
            pool.getConnection(function (err, connection) {
                if (err) {
                    console.log(err);
                    return;
                }
                connection.query("SELECT * FROM users WHERE role='teacher' AND room='" + room_Occupied[socket.id] + "'", function (err, rows) {
                    if (err) throw err;
                    if (rows.length >= 1) {
                        socket.emit('teacher is already logged in');
                    }
                    else {
                        connection.query("SELECT * FROM admin WHERE user_name='" + data.tname + "'", function (err, passver) {
                            if (err) throw err;
                            console.log(room_Occupied[socket.id]);
                            console.log("SELECT id FROM quiz WHERE roomname='" + room_Occupied[socket.id] + "' AND user_id='" + passver[0].id + "'");
                            connection.query("SELECT id FROM quiz WHERE roomname='" + room_Occupied[socket.id] + "' AND user_id='" + passver[0].id + "'", function (err, quizauthor) {
                                if (err) throw err;
                                console.log(quizauthor.length);
                                if(quizauthor.length > 0){
                                    finalNodeGeneratedHash = passver[0].user_pass.replace('$2y$', '$2a$');
                                    if (bcrypt.compareSync(data.code, finalNodeGeneratedHash)) {
                                        connection.query("INSERT INTO users (username,role,socket_id, is_admin, room) VALUES ('" + data.tname + "', 'teacher','" + socket.id + "', '1', '" + room_Occupied[socket.id] + "')", function (err, rows) {
                                            if (err) throw err;
                                            // reset question ID here
                                            ques_id[ns] = 0;
                                        });
                                        if (data.tname) {
                                            if(curr_quiz_type == 'FixedTime'){
                                                connection.query("SELECT id, question, img_path FROM quiz_questions WHERE quiz_id = '" + quiz_id[ns] + "'", function (err, allquestions) {
                                                    if (err) throw err;
                                                    console.log('came to fixedtime stuff');
                                                    for(var i=0; i < allquestions.length; i++){
                                                        corrects_given[ns][i] = 0;
                                                        incorrects_given[ns][i] = 0;
                                                        partial_corrects_given[ns][i] = 0;
                                                        console.log('corrects_given[' + ns + '][' + i + ']');
                                                        console.log(corrects_given[ns][i] + incorrects_given[ns][i] + partial_corrects_given[ns][i]);
                                                    }
                                                });
                                                teacher_socket = socket.id;
                                                if (localStorage) {
                                                    localStorage.setItem('role' + socket.id, 'teacher');
                                                    console.log(localStorage.getItem('role'+socket.id));
                                                }
                                                else {
                                                    console.log('local storage is not supported');
                                                }
                                            }
                                            console.log("SELECT keyword FROM yourls_url WHERE title='" + quiz_name[room_Occupied[socket.id]] + "'");
                                            connection.query("SELECT keyword FROM yourls_url WHERE title='" + quiz_name[room_Occupied[socket.id]] + "'", function (err, urlid) {
                                                if (err) throw err;
                                                // console.log("keyword is : " +urlid[0].keyword);
                                                if(urlid.length > 0) {
                                                    socket.emit('teacher connected', {
                                                        name: data.tname,
                                                        type: curr_quiz_type,
                                                        id: urlid[0].keyword
                                                    });
                                                }
                                                else {
                                                    socket.emit('teacher connected', {
                                                        name: data.tname,
                                                        type: curr_quiz_type,
                                                        id: 'NULL'
                                                    });
                                                }
                                            });

                                            // socket.emit('teacher connected', {name: data.tname, type: curr_quiz_type, id: quiz_id[ns]});
                                        }
                                    }
                                    else {
                                        socket.emit('error in password verification');
                                    }
                                }
                                else {
                                    socket.emit('error in password verification');
                                }
                            });
                        });
                    }
                });
            });
        });
    });
}
