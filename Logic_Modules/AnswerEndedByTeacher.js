var pool = require('../config/DatabaseSettings').pool();

module.exports = function (nsp) {
    nsp.on('connection', function (socket) {
        socket.on('answer ended by teacher', function () {
            pool.getConnection(function (err, connection) {
                if (err) {
                    console.log(err);
                    return;
                }
                connection.query("SELECT username,score FROM users WHERE is_admin='0' AND room='" + room_Occupied[socket.id] + "' ORDER BY score DESC", function (err, rows) {
                    if (err) throw err;
                    console.log(rows[0].score);
                    connection.query("SELECT img_path FROM quiz_questions WHERE id = '" + current_question_id[room_Occupied[socket.id]] + "'", function (err, img) {
                        if (err) throw err;
                        connection.query("SELECT is_true FROM quiz_answers WHERE ques_id = '" + current_question_id[room_Occupied[socket.id]] + "'", function (err, totalanswers) {
                            if (err) throw err;
                            socket.emit('hide single question div');
                            nsp.in(room_Occupied[socket.id]).emit("leader board", {
                                data: rows,
                                que_id: ques_id[room_Occupied[socket.id]],
                                img: img,
                                last_q_id: current_question_id[room_Occupied[socket.id]],
                                total_answers: totalanswers,
                                is_poll: is_poll
                            });
                            socket.to(room_Occupied[socket.id]).emit('make chart as modal now', {
                                ques: ques_id[room_Occupied[socket.id]], A: givenAnsA[ns],
                                B: givenAnsB[ns],
                                C: givenAnsC[ns],
                                D: givenAnsD[ns]
                            });
                            // ques_id[room_Occupied[socket.id]]++;
                            if (is_traverse == 1) {
                                console.log('is_traverse');
                                socket.emit('show next question option', {
                                    ques_id: ques_id[room_Occupied[socket.id]],
                                    prev_q: 1
                                });
                            }
                            else {
                                console.log('not is_traverse');
                                socket.emit('show next question option', {
                                    ques_id: ques_id[room_Occupied[socket.id]],
                                    prev_q: 0
                                });
                            }
                            nsp.in(room_Occupied[socket.id]).emit('modify last leader board', {is_poll: is_poll});
                            socket.emit('show result chart', {
                                corrects: corrects_given[room_Occupied[socket.id]],
                                incorrects: incorrects_given[room_Occupied[socket.id]],
                                AGiven: givenAnsA[ns],
                                BGiven: givenAnsB[ns],
                                CGiven: givenAnsC[ns],
                                DGiven: givenAnsD[ns],
                                is_poll: is_poll
                            });
                            socket.to(room_Occupied[socket.id]).emit('stop displaying the result status to students');
                        });
                    });
                });
            });
        });
    });
}
