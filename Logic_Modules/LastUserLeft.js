var pool = require('../config/DatabaseSettings').pool();

module.exports = function(nsp) {
    nsp.on('connection', function (socket) {
        socket.on('Check for last user in server', function () {
            pool.getConnection(function (err, connection) {
                if (err) {
                    console.log(err);
                    return;
                }
                connection.query('SELECT * FROM users WHERE is_admin="0" AND room="' + room_Occupied[socket.id] + '"', function (err, rows) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                    if (rows.length == 0) {
                        socket.emit('no students left in the quiz', rows);
                    }
                });
                connection.query('SELECT * FROM users WHERE is_admin="1" AND room="' + room_Occupied[socket.id] + '"', function (err, rows) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                    if (rows.length == 0) {
                        socket.emit('no teachers left in the quiz', rows);
                    }
                });

            });
        });
    });
}