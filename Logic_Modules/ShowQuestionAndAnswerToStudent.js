var pool = require('../config/DatabaseSettings').pool();

module.exports = function(nsp) {
    nsp.on('connection', function (socket) {
        socket.on('show q', function (data) {
            var LocalStorage = require('node-localstorage').LocalStorage;
            localStorage = new LocalStorage('./scratch');
            var back;
            var u_name;
            // var backCurrQId;
            pool.getConnection(function (err, connection) {
                if (err) {
                    console.log(err);
                    return;
                }

                if(is_traverse == 0) {
                    console.log('cuss ques id: ' + ques_id[room_Occupied[socket.id]]);
                    connection.query("SELECT id,question,img_path FROM quiz_questions WHERE quiz_id = '" + quiz_id[room_Occupied[socket.id]] + "' LIMIT " + (ques_id[room_Occupied[socket.id]] - 1) + ",1", function (err, rows) {
                        if (err) throw err;
                        backCurrQId = current_question_id[room_Occupied[socket.id]];
                        current_question_id[room_Occupied[socket.id]] = rows[0].id;
                        socket.emit('show question', {
                            row: rows[0],
                            ques_id: ques_id[room_Occupied[socket.id]],
                            oldID: backCurrQId
                        });
                    });
                }
                else{
                    // breakdown of big index
                    // quiz_questions has all questions with index of NS = roomname of quiz for that [roomOccupied[socket.id]]
                    // ques_id is current number of question that's going on. This is used to get index of array of quiz_questions where all questions are stored.
                    // ques_id starts from 1 and index starts from 0 thats why (-1)
                    // .id is to get id attribute of array element

                    // to fix the overwriting of values in back
                    // if(current_question_id[room_Occupied[socket.id]]) {
                    //     if(!current_question_id[room_Occupied[socket.id] + 'back']) {
                    //         current_question_id[room_Occupied[socket.id] + 'back'] = current_question_id[room_Occupied[socket.id]];
                    //         console.log('back: ' + current_question_id[room_Occupied[socket.id] + 'back']);
                    //
                    //     }
                    // }

                    back = [ques_id[room_Occupied[socket.id]]] - 1;

                    current_question_id[room_Occupied[socket.id]] = quiz_questions[[room_Occupied[socket.id]]][([ques_id[room_Occupied[socket.id]]] - 1)].id;

                    if(localStorage.getItem('last_score' + socket.id + ':' + ques_id[room_Occupied[socket.id]])){
                        // remvoing scores added when same question was submitted last time
                        var score_last_time = localStorage.getItem('last_score' + socket.id + ':' + ques_id[room_Occupied[socket.id]]);
                        console.log('last score: ' + score_last_time);
                        connection.query("UPDATE users SET score = score - "+ score_last_time +" WHERE socket_id ='" + socket.id + "' AND room='" + room_Occupied[socket.id] +"' AND is_admin='0'", function (err, scoress) {
                            if (err) throw err;
                        });

                        connection.query("SELECT username FROM users WHERE socket_id='" + socket.id + "'", function (err, username) {
                            if (err) throw err;
                            u_name = username[0].username;
                        });

                        connection.query("UPDATE final_results SET total_score = total_score - "+ score_last_time +" where quiz_id='" + quiz_id[room_Occupied[socket.id]] + "' AND username='" + u_name + "'", function (err, scoress) {
                            if (err) throw err;
                        });
                    }

                    if(data){
                        if(data.goback == 1) {
                            // if(!localStorage.getItem('last_score'+socket.id)){
                            //     localStorage.setItem('last_score'+socket.id, 0);
                            // }
                            // connection.query("UPDATE users SET score = score - "+ localStorage.getItem('last_score'+socket.id) +" WHERE socket_id ='" + socket.id + "' AND room='" + room_Occupied[socket.id] +"' AND is_admin='0'", function (err, scoress) {
                            //     if (err) throw err;
                            // });
                            //
                            // connection.query("SELECT username FROM users WHERE socket_id='" + socket.id + "'", function (err, username) {
                            //     if (err) throw err;
                            //     u_name = username[0].username;
                            // });
                            //
                            // connection.query("UPDATE final_results SET total_score = total_score - "+ localStorage.getItem('last_score'+socket.id) +" where quiz_id='" + quiz_id[room_Occupied[socket.id]] + "' AND username='" + u_name + "'", function (err, scoress) {
                            //     if (err) throw err;
                            // });
                            console.log('clicked on prev question');
                            current_question_id[room_Occupied[socket.id] + 'back'] = current_question_id[room_Occupied[socket.id]];
                            console.log('back: ' + current_question_id[room_Occupied[socket.id] + 'back']);
                            back = [ques_id[room_Occupied[socket.id]]];
                        }
                    }

                    console.log('back: ' + back);
                    console.log('ques_id when showing question: ' + ques_id[room_Occupied[socket.id]]);

                    socket.emit('show question', {
                        row: quiz_questions[[room_Occupied[socket.id]]][([ques_id[room_Occupied[socket.id]]] - 1)],
                        ques_id: ques_id[room_Occupied[socket.id]],
                        oldID: back
                    });
                }
            });
        });

        socket.on('show a',function(){
            pool.getConnection(function(err,connection){
                if (err) {
                    console.log(err);
                    return;
                }
                console.log('in answers: ' + current_question_id[room_Occupied[socket.id]]);
                connection.query("SELECT answer,ques_id,is_true FROM quiz_answers WHERE ques_id = '"+current_question_id[room_Occupied[socket.id]]+"'",function(err,ans){
                    if(err) console.log(err);
                    socket.emit('show answers', {ans: ans});
                });
            });
        });

        socket.on('reset choices selected callback', function () {
            givenAnsA[ns] = 0;
            givenAnsB[ns] = 0;
            givenAnsC[ns] = 0;
            givenAnsD[ns] = 0;
        });

        socket.on('reset corrects and incorrects callback', function () {
            corrects_given[room_Occupied[socket.id]] = 0;
            incorrects_given[room_Occupied[socket.id]] = 0;
        });
    });
}