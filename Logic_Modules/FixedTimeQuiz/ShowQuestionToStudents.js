var pool = require('../../config/DatabaseSettings').pool();

module.exports = function(nsp) {
    nsp.on('connection', function (socket) {
        socket.on('show question to students', function (data) {

            var LocalStorage = require('node-localstorage').LocalStorage;
            localStorage = new LocalStorage('./scratch');
            var q_number = parseInt(localStorage.getItem('ques_id' + socket.id));

            if(data){
                console.log('there is data: ' + data.jump_to);
                if(data.jump_to >= 0){
                    console.log('jump_to: ' + data.jump_to);
                    q_number = data.jump_to;
                    localStorage.setItem('ques_id' + socket.id, q_number);
                }
            }

            if(q_number == 0 && !localStorage.getItem('score_of_' + q_number + socket.id)){
                quiz_started[room_Occupied[socket.id]] = 1;
                if(socket.id != teacher_socket)
                    socket.emit('show jump links to students', {all_questions: quiz_questions[room_Occupied[socket.id]]});
                else
                    socket.to(room_Occupied[socket.id]).emit('show jump links to students', {all_questions: quiz_questions[room_Occupied[socket.id]]});
            }
            else{
            }

            console.log('q number at show ques' + q_number);
            var back;
            var u_name;
            // var backCurrQId;

            console.log('local score of this: ' + localStorage.getItem('score_of_' + q_number + socket.id));

            if(localStorage.getItem('score_of_' + q_number + socket.id) != null){
                console.log('he already gave this ans... duhhhh!!');
            }

            pool.getConnection(function (err, connection) {
                if (err) {
                    console.log(err);
                    return;
                }
                // breakdown of big index
                // quiz_questions has all questions with index of NS = roomname of quiz for that [roomOccupied[socket.id]]
                // ques_id is current number of question that's going on. This is used to get index of array of quiz_questions where all questions are stored.
                // ques_id starts from 1 and index starts from 0 thats why (-1)
                // .id is to get id attribute of array element

                back = [ques_id[room_Occupied[socket.id]]] - 1;

                console.log('corrects_given[' + room_Occupied[socket.id] + '][' + q_number + ']');
                console.log(corrects_given[room_Occupied[socket.id]][q_number]);


                console.log('Big friggin array test: ' + quiz_questions[[room_Occupied[socket.id]]][q_number].id)

                localStorage.setItem('curr_question_id' + socket.id, quiz_questions[[room_Occupied[socket.id]]][q_number].id);

                if(localStorage.getItem('score_of_' + q_number + socket.id)){
                    // remvoing scores added when same question was submitted last time
                    var score_last_time = localStorage.getItem('score_of_' + q_number + socket.id);
                    console.log('last score: ' + score_last_time);
                    connection.query("UPDATE users SET score = score - "+ score_last_time +" WHERE socket_id ='" + socket.id + "' AND room='" + room_Occupied[socket.id] +"' AND is_admin='0'", function (err, scoress) {
                        if (err) throw err;
                    });

                    connection.query("SELECT username FROM users WHERE socket_id='" + socket.id + "'", function (err, username) {
                        if (err) throw err;
                        u_name = username[0].username;
                    });

                    connection.query("UPDATE final_results SET total_score = total_score - "+ score_last_time +" where quiz_id='" + quiz_id[room_Occupied[socket.id]] + "' AND username='" + u_name + "'", function (err, scoress) {
                        if (err) throw err;
                    });
                }

                // if(data){
                //     if(data.goback == 1) {
                //         console.log('clicked on prev question');
                //         current_question_id[room_Occupied[socket.id] + 'back'] = current_question_id[room_Occupied[socket.id]];
                //         console.log('back: ' + current_question_id[room_Occupied[socket.id] + 'back']);
                //         back = [ques_id[room_Occupied[socket.id]]];
                //     }
                // }

                // console.log('back: ' + back);
                // console.log('ques_id when showing question: ' + ques_id[room_Occupied[socket.id]]);

                if(socket.id != teacher_socket){
                    socket.emit('show question', {
                        row: quiz_questions[[room_Occupied[socket.id]]][q_number],
                        ques_id: q_number,
                        oldID: back,
                        type: 'FixedTime'
                    });
                }
                else {
                    socket.to(room_Occupied[socket.id]).emit('show question', {
                        row: quiz_questions[[room_Occupied[socket.id]]][q_number],
                        ques_id: q_number,
                        oldID: back,
                        type: 'FixedTime'
                    });
                }


            });
        });

        socket.on('show answers to students',function(){
            var LocalStorage = require('node-localstorage').LocalStorage;
            localStorage = new LocalStorage('./scratch');
            var q_number = localStorage.getItem('ques_id' + socket.id);

            var choice_A = localStorage.getItem('A_selected_' + q_number + socket.id);
            var choice_B = localStorage.getItem('B_selected_' + q_number + socket.id);
            var choice_C = localStorage.getItem('C_selected_' + q_number + socket.id);
            var choice_D = localStorage.getItem('D_selected_' + q_number + socket.id);

            pool.getConnection(function(err,connection){
                if (err) {
                    console.log(err);
                    return;
                }
                console.log('in answers: ' + quiz_questions[[room_Occupied[socket.id]]][q_number].id);
                connection.query("SELECT answer,ques_id,is_true FROM quiz_answers WHERE ques_id = '"+quiz_questions[[room_Occupied[socket.id]]][q_number].id+"'",function(err,ans){
                    if(err) console.log(err);
                    if(socket.id != teacher_socket) {
                        socket.emit('show answers', {ans: ans, A: choice_A, B: choice_B, C: choice_C, D: choice_D});
                    }
                    else {
                        socket.to(room_Occupied[socket.id]).emit('show answers', {ans: ans, A: choice_A, B: choice_B, C: choice_C, D: choice_D});
                    }
                });
            });
        });

        socket.on('reset choices selected callback', function () {
            givenAnsA[ns] = 0;
            givenAnsB[ns] = 0;
            givenAnsC[ns] = 0;
            givenAnsD[ns] = 0;
        });

        socket.on('reset corrects and incorrects callback', function () {
            corrects_given[room_Occupied[socket.id]] = 0;
            incorrects_given[room_Occupied[socket.id]] = 0;
        });
    });
}