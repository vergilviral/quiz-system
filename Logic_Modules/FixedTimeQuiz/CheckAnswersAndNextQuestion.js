var pool = require('../../config/DatabaseSettings').pool();

module.exports = function(nsp) {
    nsp.on('connection', function (socket) {
        socket.on('check answer and show next question', function (answers) {
            var LocalStorage = require('node-localstorage').LocalStorage;
            localStorage = new LocalStorage('./scratch');
            var curr_q_id = localStorage.getItem('curr_question_id'+ socket.id);
            var q_number = parseInt(localStorage.getItem('ques_id'+ socket.id));
            var status = new Array();
            var answersArr = new Array();
            var answersMatched = 0;
            var answersNotMatched = 0;
            var flag = 0;
            var corrects = new Array();
            var u_name = '';
            var score = 0;
            var partial_marks = 0;
            var counter = 0;
            var total_added = 0;
            var is_first = '';
            var last_status = localStorage.getItem('status_of_' + q_number + socket.id);
            var last_score = localStorage.getItem('score_of_' + q_number + socket.id);

            localStorage.setItem('ques_id'+ socket.id, (q_number+1));

            if(last_status == 'correct'){
                corrects_given[room_Occupied[socket.id]][q_number]--;
            }
            if(last_status == 'incorrect'){
                incorrects_given[room_Occupied[socket.id]][q_number]--;
            }
            if(last_status == 'partial_correct'){
                partial_corrects_given[room_Occupied[socket.id]][q_number]--;
            }

            status[0] = answers.ansstatus1;
            status[1] = answers.ansstatus2;
            status[2] = answers.ansstatus3;
            status[3] = answers.ansstatus4;
            answersArr[0] = answers.answer1;
            answersArr[1] = answers.answer2;
            answersArr[2] = answers.answer3;
            answersArr[3] = answers.answer4;
            if(status[0] == true){
                givenAnsA[ns]++;
                localStorage.setItem('A_selected_' + q_number + socket.id, 'true');
            }
            else {
                localStorage.setItem('A_selected_' + q_number + socket.id, 'false');
            }
            if(status[1] == true){
                givenAnsB[ns]++;
                localStorage.setItem('B_selected_' + q_number + socket.id, 'true');
            }
            else {
                localStorage.setItem('B_selected_' + q_number + socket.id, 'false');
            }
            if(status[2] == true){
                givenAnsC[ns]++;
                localStorage.setItem('C_selected_' + q_number + socket.id, 'true');
            }
            else {
                localStorage.setItem('C_selected_' + q_number + socket.id, 'false');
            }
            if(status[3] == true){
                givenAnsD[ns]++;
                localStorage.setItem('D_selected_' + q_number + socket.id, 'true');
            }
            else {
                localStorage.setItem('D_selected_' + q_number + socket.id, 'false');
            }
            pool.getConnection(function (err, connection) {
                if (err) {
                    callback(false);
                    return;
                }

                console.log('curr q id: ' + curr_q_id);

                connection.query("SELECT partial_marking FROM quiz_questions WHERE id = '" + curr_q_id + "'", function (err, pm) {
                    if (err) throw err;
                    if(pm[0].partial_marking == 1){
                        partial_marks = 1;
                    }
                });

                connection.query("SELECT is_true FROM quiz_answers WHERE ques_id = '" + curr_q_id + "'", function (err, rows) {
                    if (err) throw err;

                    for(x = 0; x < 4; x++) {
                        if (rows[x].is_true == 1) {
                            if (status[x] == true) {
                                corrects[x] = 1;
                                score++;
                                counter++;
                                console.log('match');
                            }
                            else {
                                corrects[x] = 2;
                                answersMatched--;
                                counter++;
                            }
                        }
                        else {
                            if (status[x] == true) {
                                corrects[x] = -1;
                                answersNotMatched++;
                                score -= (1/2);
                                console.log('not match');
                            }
                            else {
                                corrects[x] = 0;
                            }
                        }
                    }

                    console.log('this is score divided by counter' + ((score/counter)*100));

                    connection.query("SELECT username FROM users WHERE socket_id='" + socket.id + "'", function (err, username) {
                        if (err) throw err;
                        u_name = username[0].username;
                        console.log('u_name here: ' + u_name);
                        nsp.in(room_Occupied[socket.id]).emit("answer submitted by student snackbar", {
                            username: u_name
                        });
                        console.log("answers submitted by student: " + q_number);

                        if (answersMatched >= 0 && answersNotMatched == 0) {
                            total_added = 100;
                            console.log('all correct');
                            localStorage.setItem('status_of_' + q_number + socket.id, 'correct');
                            corrects_given[room_Occupied[socket.id]][q_number]++;
                            if(corrects_given[room_Occupied[socket.id]][q_number] == 1){
                                console.log('it came here though');
                                is_first = u_name;
                            }
                            else{
                                console.log('came here: ' + corrects_given[room_Occupied[socket.id]][q_number]);
                            }
                            // corrects_given[ns]++;
                            connection.query("UPDATE quiz SET total_rights = total_rights + 1 where roomname='" + room_Occupied[socket.id] + "'", function (err, totalstus) {
                                if (err) throw err;
                            });
                            connection.query("UPDATE final_results SET total_score = total_score + 100 where quiz_id='" + quiz_id[room_Occupied[socket.id]] + "' AND username='" + u_name + "'", function (err, totalstus) {
                                if (err) throw err;
                            });
                            // socket.emit('singleresult', {flag: 1, que_id: current_question_id[room_Occupied[socket.id]], correct: corrects, qid: q_number});
                            // flag++;
                            connection.query("UPDATE users SET score = score + 100 WHERE socket_id ='" + socket.id + "' AND room='" + room_Occupied[socket.id] +"'", function (err, scoress) {
                                if (err) throw err;
                            });
                        }
                        else if(partial_marks == 0 && answersNotMatched >= 0) {
                            localStorage.setItem('status_of_' + q_number + socket.id, 'incorrect');
                            incorrects_given[room_Occupied[socket.id]][q_number]++;
                            total_added = 0;
                            console.log('wrong answers');
                            // incorrects_given[ns]++;
                            // socket.emit("singleresult", {flag: 0, que_id: current_question_id[room_Occupied[socket.id]], correct: corrects, qid: q_number})
                        }
                        else if(partial_marks == 1 && score >=0) {
                            localStorage.setItem('status_of_' + q_number + socket.id, 'partial_correct');
                            partial_corrects_given[room_Occupied[socket.id]][q_number]++;
                            total_added = ((score/counter)*100);
                            console.log('partial correct');
                            connection.query("UPDATE final_results SET total_score = total_score + "+ ((score/counter)*100) +" where quiz_id='" + quiz_id[room_Occupied[socket.id]] + "' AND username='" + u_name + "'", function (err, totalstus) {
                                if (err) throw err;
                            });
                            // socket.emit('singleresult', {flag: 1, que_id: current_question_id[room_Occupied[socket.id]], correct: corrects, qid: q_number});
                            // flag++;
                            connection.query("UPDATE users SET score = score + "+ ((score/counter)*100) +" WHERE socket_id ='" + socket.id + "' AND room='" + room_Occupied[socket.id] +"'", function (err, scoress) {
                                if (err) throw err;
                            });
                        }
                        else{
                            incorrects_given[room_Occupied[socket.id]][q_number]++;
                            total_added = 0;
                            console.log('partial on but wrong answers');
                        }

                        console.log('ques_id when submitting answer: ' + q_number);
                        if (localStorage) {
                            localStorage.setItem('score_of_' + q_number + socket.id, total_added);
                        }
                        else {
                            console.log('local storage is not supported');
                        }

                        console.log('correct: ' + corrects_given[room_Occupied[socket.id]][q_number]);
                        console.log('incorrect: ' + incorrects_given[room_Occupied[socket.id]][q_number]);
                        console.log('par_correct: ' + partial_corrects_given[room_Occupied[socket.id]][q_number]);
                        console.log('is_first: ' + is_first);
                        console.log('username: ' + u_name);

                        if(!quiz_questions[[room_Occupied[socket.id]]][(q_number+1)]){
                            socket.emit('quiz ended fixedtime');
                        }
                        else {
                            socket.emit('show next question to students');
                        }

                        // for(var i=0; i<quiz_questions[room_Occupied[socket.id]].length; i++){
                        //
                        // }

                        var arr = incorrects_given[room_Occupied[socket.id]];

                        var i = indexOfMax(arr);

                        function indexOfMax(arr) {
                            if (arr.length === 0) {
                                return -1;
                            }

                            var max = arr[0];
                            var maxIndex = 0;

                            for (var i = 1; i < arr.length; i++) {
                                if (arr[i] > max) {
                                    maxIndex = i;
                                    max = arr[i];
                                }
                            }

                            return maxIndex;
                        }

                        console.log('Most incorrect at: ' + i);

                        socket.to(teacher_socket).emit('update stats', {correct: corrects_given[room_Occupied[socket.id]][q_number],
                            incorrect: incorrects_given[room_Occupied[socket.id]][q_number],
                            partial_correct: partial_corrects_given[room_Occupied[socket.id]][q_number],
                            q_id: q_number,
                            is_first: is_first,
                            user: localStorage.getItem('username'+socket.id),
                            score: total_added,
                            last_score: last_score,
                            toughestq: i});

                    });
                });
            });
        });
    });
}