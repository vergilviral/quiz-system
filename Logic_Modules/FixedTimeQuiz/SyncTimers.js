var pool = require('../../config/DatabaseSettings').pool();

module.exports = function(nsp) {
    nsp.on('connection', function (socket) {
        socket.on('sync all timers', function (data) {
            pool.getConnection(function (err, connection) {
                if (err) {
                    console.log(err);
                    return;
                }
                var elapsed_seconds = data.time;
                var syncTimes = setInterval(function() {
                    elapsed_seconds = elapsed_seconds - 1;
                    if(elapsed_seconds > 0)
                        nsp.in(room_Occupied[socket.id]).emit('sync timer', {time: elapsed_seconds});
                    else {
                        nsp.in(room_Occupied[socket.id]).emit('sync timer', {time: elapsed_seconds,});
                        clearInterval(syncTimes);
                    }
                }, 1000);
            });
        });
    });
}