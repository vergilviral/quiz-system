var pool = require('../../config/DatabaseSettings').pool();

module.exports = function(nsp) {
    nsp.on('connection', function (socket) {
        socket.on('End Quiz Now', function () {
            if(socket.id != teacher_socket)
                socket.emit('end the quiz for students');
            else
                socket.emit('end the quiz for teacher');
        });
    });
}