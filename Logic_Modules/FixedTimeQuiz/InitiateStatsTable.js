var pool = require('../../config/DatabaseSettings').pool();

module.exports = function(nsp) {
    nsp.on('connection', function (socket) {
        socket.on('fill table with question numbers', function () {
            socket.emit('add question numbers in table', {total_questions: quiz_questions[room_Occupied[socket.id]]});
            pool.getConnection(function (err, connection) {
                if (err) {
                    console.log(err);
                    return;
                }
                connection.query("SELECT username,score FROM users WHERE is_admin='0' AND room='" + room_Occupied[socket.id] + "' ORDER BY score DESC", function (err, rows) {
                    if (err) throw err;
                    socket.emit('initiate live leaderboard', rows);
                });
                connection.query("SELECT time FROM quiz WHERE roomname='" + room_Occupied[socket.id] + "'", function (err, time) {
                    if (err) throw err;
                    timer[room_Occupied[socket.id]] = time[0].time;
                    // nsp.in(room_Occupied[socket.id]).emit('start timer', time[0].time);
                    nsp.in(room_Occupied[socket.id]).emit('start timer', 10000);
                });
            });
        });
    });
}