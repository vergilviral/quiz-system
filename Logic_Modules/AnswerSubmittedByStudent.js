var pool = require('../config/DatabaseSettings').pool();

module.exports = function(nsp) {
    nsp.on('connection', function (socket) {
        socket.on('answer submitted', function (answers) {
            var LocalStorage = require('node-localstorage').LocalStorage;
            localStorage = new LocalStorage('./scratch');
            var status = new Array();
            var answersArr = new Array();
            var answersMatched = 0;
            var answersNotMatched = 0;
            var flag = 0;
            var corrects = new Array();
            var u_name;
            var score = 0;
            var partial_marks = 0;
            var counter = 0;
            var total_added = 0;
            status[0] = answers.ansstatus1;
            status[1] = answers.ansstatus2;
            status[2] = answers.ansstatus3;
            status[3] = answers.ansstatus4;
            answersArr[0] = answers.answer1;
            answersArr[1] = answers.answer2;
            answersArr[2] = answers.answer3;
            answersArr[3] = answers.answer4;
            if(status[0] == true){
                givenAnsA[ns]++;
            }
            if(status[1] == true){
                givenAnsB[ns]++;
            }
            if(status[2] == true){
                givenAnsC[ns]++;
            }
            if(status[3] == true){
                givenAnsD[ns]++;
            }
            pool.getConnection(function (err, connection) {
                if (err) {
                    callback(false);
                    return;
                }
                connection.query("SELECT partial_marking FROM quiz_questions WHERE id = '" + current_question_id[room_Occupied[socket.id]] + "'", function (err, pm) {
                    if (err) throw err;
                    if(pm[0].partial_marking == 1){
                        partial_marks = 1;
                    }
                });
                    connection.query("SELECT is_true FROM quiz_answers WHERE ques_id = '" + current_question_id[room_Occupied[socket.id]] + "'", function (err, rows) {
                    if (err) throw err;

                    for(x = 0; x < 4; x++) {
                        if (rows[x].is_true == 1) {
                            if (status[x] == true) {
                                corrects[x] = 1;
                                score++;
                                counter++;
                            }
                            else {
                                corrects[x] = 2;
                                answersMatched--;
                                counter++;
                            }
                        }
                        else {
                            if (status[x] == true) {
                                corrects[x] = -1;
                                answersNotMatched++;
                                score -= (1/2);
                            }
                            else {
                                corrects[x] = 0;
                            }
                        }
                    }

                    console.log('this is score divided by counter' + ((score/counter)*100));

                    connection.query("SELECT username FROM users WHERE socket_id='" + socket.id + "'", function (err, username) {
                        if (err) throw err;
                        u_name = username[0].username;
                        nsp.in(room_Occupied[socket.id]).emit("answer submitted by student snackbar", {
                            username: u_name
                        });
                    });


                    console.log("answers submitted by student: " +ques_id[room_Occupied[socket.id]]);

                    if (answersMatched >= 0 && answersNotMatched == 0) {
                        total_added = 100;
                        console.log('all correct');
                        corrects_given[ns]++;
                        connection.query("UPDATE quiz SET total_rights = total_rights + 1 where roomname='" + room_Occupied[socket.id] + "'", function (err, totalstus) {
                            if (err) throw err;
                        });
                        connection.query("UPDATE final_results SET total_score = total_score + 100 where quiz_id='" + quiz_id[room_Occupied[socket.id]] + "' AND username='" + u_name + "'", function (err, totalstus) {
                            if (err) throw err;
                        });
                        socket.emit('singleresult', {flag: 1, que_id: current_question_id[room_Occupied[socket.id]], correct: corrects, qid: ques_id[room_Occupied[socket.id]], is_poll: is_poll});
                        // flag++;
                        connection.query("UPDATE users SET score = score + 100 WHERE socket_id ='" + socket.id + "' AND room='" + room_Occupied[socket.id] +"'", function (err, scoress) {
                            if (err) throw err;
                            console.log('Score updated woohoo!');
                        });
                        socket.emit('set correct var', {correct: true});
                    }
                    else if(partial_marks == 0 && answersNotMatched >= 0) {
                        total_added = 0;
                        console.log('wrong answers');
                        incorrects_given[ns]++;
                        socket.emit("singleresult", {flag: 0, que_id: current_question_id[room_Occupied[socket.id]], correct: corrects, qid: ques_id[room_Occupied[socket.id]], is_poll: is_poll})
                        socket.emit('set correct var', {correct: false});
                    }
                    else if(partial_marks == 1 && score >=0) {
                        total_added = ((score/counter)*100);
                        console.log('partial correct');
                        connection.query("UPDATE final_results SET total_score = total_score + "+ ((score/counter)*100) +" where quiz_id='" + quiz_id[room_Occupied[socket.id]] + "' AND username='" + u_name + "'", function (err, totalstus) {
                            if (err) throw err;
                        });
                        socket.emit('singleresult', {flag: 1, que_id: current_question_id[room_Occupied[socket.id]], correct: corrects, qid: ques_id[room_Occupied[socket.id]], is_poll: is_poll});
                        // flag++;
                        connection.query("UPDATE users SET score = score + "+ ((score/counter)*100) +" WHERE socket_id ='" + socket.id + "' AND room='" + room_Occupied[socket.id] +"'", function (err, scoress) {
                            if (err) throw err;
                        });
                    }

                    if(is_traverse == 1) {
                        console.log('ques_id when submitting answer: ' + ques_id[room_Occupied[socket.id]]);
                        if (localStorage) {
                            localStorage.setItem('last_score' + socket.id + ':' + ques_id[room_Occupied[socket.id]], total_added);
                        }
                        else {
                            console.log('local storage is not supported');
                        }
                    }

                    nsp.in(room_Occupied[socket.id]).emit("refresh student choices chart", {A: givenAnsA[ns], B: givenAnsB[ns], C: givenAnsC[ns], D: givenAnsD[ns], ques_id: ques_id[room_Occupied[socket.id]]});
                });
            });
        });
    });
}