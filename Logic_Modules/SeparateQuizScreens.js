module.exports = function(nsp) {
    nsp.on('connection', function (socket) {
        socket.on('dividequiz screens', function (data) {
            if(data){
                if(data.prev_q == 1) {
                    socket.emit("quiz screen for teacher", {goback: 1});
                    socket.to(room_Occupied[socket.id]).emit("quiz screen for students", {goback: 1});
                }
                if(data.jump_q >= 0) {
                    console.log('atleast came here');
                    ques_id[room_Occupied[socket.id]] = data.jump_q + 1;
                    console.log('Jump here: ' + ques_id[room_Occupied[socket.id]]);
                    socket.emit("quiz screen for teacher");
                    socket.to(room_Occupied[socket.id]).emit("quiz screen for students");
                }
            }
            else {
                ques_id[room_Occupied[socket.id]]++;
                console.log('ques_id in divide screen' + ques_id[room_Occupied[socket.id]]);
                socket.emit("quiz screen for teacher");
                socket.to(room_Occupied[socket.id]).emit("quiz screen for students");
            }
        });
    });
}