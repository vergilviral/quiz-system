var pool = require('../config/DatabaseSettings').pool();
var teacher_available=[];

module.exports = function(nsp) {
    nsp.on('connection', function(socket){
        socket.on('add student', function(username){
            var LocalStorage = require('node-localstorage').LocalStorage;
            localStorage = new LocalStorage('./scratch');
            pool.getConnection(function(err,connection){
                connection.query('SELECT * FROM users WHERE is_admin="1" and room="' + room_Occupied[socket.id] + '"',function(err,rows){
                    if(err) {
                        console.log(err);
                        throw err;
                    }
                    if(rows.length == 0){
                        socket.emit('please wait for teacher to login', rows);
                    }
                    else{
                        if(!username.trim()) {
                            socket.emit('please enter proper username');
                        }
                        else {
                            connection.query("SELECT * FROM users WHERE username='" + username + "' AND room='" + room_Occupied[socket.id] + "'", function (err, rows) {
                                if (err) throw err;
                                if (rows.length >= 1) {
                                    socket.emit('please select different username');
                                }
                                else {
                                    connection.query("SELECT username FROM final_results WHERE quiz_id = '"+ quiz_id[room_Occupied[socket.id]] + "' AND username='" + username + "'", function (err, alreadyGiven) {
                                        if (err) throw err;
                                        if (alreadyGiven.length >= 1) {
                                            socket.emit('please select different username');
                                        }
                                        else{
                                            connection.query("INSERT INTO users (username,socket_id,room, score) VALUES ('" + username + "','" + socket.id + "', '" + room_Occupied[socket.id] + "', 0)", function (err, rows) {
                                                if (err) throw err;
                                                connection.query("UPDATE quiz SET total_students_joined = total_students_joined + 1 where roomname='" + room_Occupied[socket.id] + "'", function (err, totalstus) {
                                                    if (err) throw err;
                                                    connection.query("INSERT INTO final_results (username, quiz_id, total_score) VALUES ('" + username + "','" + quiz_id[room_Occupied[socket.id]] + "', 0)", function (err, rows) {
                                                        if (err) throw err;
                                                    });
                                                });
                                            });
                                            if(curr_quiz_type == 'FixedTime') {
                                                if (localStorage) {
                                                    localStorage.setItem('role' + socket.id, 'student');
                                                    localStorage.setItem('curr_question_id' + socket.id, quiz_questions[[room_Occupied[socket.id]]][0].id);
                                                    localStorage.setItem('ques_id' + socket.id, 0);
                                                    localStorage.setItem('username' + socket.id, username);
                                                    console.log(localStorage.getItem('username'+socket.id));
                                                }
                                                else {
                                                    console.log('local storage is not supported');
                                                }
                                                socket.to(teacher_socket).emit('refresh live leaderboard', username);
                                                if(quiz_started[room_Occupied[socket.id]] == 1) {
                                                    console.log('start because of here');
                                                    socket.emit('show next question to students', {first_time: 'yes'});
                                                }
                                                else {
                                                    console.log('why going to waiting now?: ' + quiz_started[room_Occupied[socket.id]]);
                                                    socket.emit('waitingnow');
                                                }
                                            }
                                            if(curr_quiz_type != 'FixedTime'){
                                                console.log('HERE??');
                                                socket.emit('waitingnow');
                                            }
                                            nsp.in(room_Occupied[socket.id]).emit('student connected', username);
                                            connection.query("SELECT * FROM users WHERE role='teacher' AND room='" + room_Occupied[socket.id] + "'",function(err,rows){
                                                if(err) throw err;
                                                connection.query("SELECT * FROM users WHERE role='student' AND room='" + room_Occupied[socket.id] + "'",function(err,students){
                                                    if(err) throw err;
                                                    if(rows.length >= 1 && students.length == 1) {
                                                        nsp.to(rows[0].socket_id).emit('display start screen to teacher now', {all_questions: quiz_questions[room_Occupied[socket.id]]});
                                                    }
                                                    else{
                                                        socket.emit('refresh the currently joined students list', students);
                                                    }
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            });

        });
        socket.on('disconnect', function(){
            var uname;
            var roomOcc = room_Occupied[socket.id];
            pool.getConnection(function(err,connection){
                if (err) {
                    //console.log(err);
                    return;
                }
                connection.query("SELECT username,socket_id FROM users WHERE socket_id = '"+ socket.id + "'",function(err,rows){
                    if(err) throw err;
                    if(rows.length != 0){
                        uname = rows[0].username;
                        //console.log('user removed with username: ' + uname);
                        nsp.in(room_Occupied[socket.id]).emit('disconnected user', uname);
                    }
                });
                connection.query("DELETE FROM users WHERE socket_id = '"+ socket.id + "'",function(err,rows){
                    if(err) throw err;
                    if(uname) {
                        nsp.in(roomOcc).emit('check if last user left');
                    }
                });
                connection.query("SELECT * FROM users WHERE role='student' AND room='" + room_Occupied[socket.id] + "'",function(err,students){
                    if(err) throw err;
                    nsp.in(room_Occupied[socket.id]).emit('refresh the currently joined students list', students);
                });
            });
        });
    });
}
