var pool = require('../config/DatabaseSettings').pool();

module.exports = function(nsp) {
    nsp.on('connection', function (socket) {
        socket.on('submit feedbacks', function () {
            socket.to(room_Occupied[socket.id]).emit('show submit feedback modal');
        });

        socket.on('feedback submitted', function (feedback) {

            var name = feedback.name;
            var email = feedback.email;
            var feedbackmsg = feedback.feedback;

            //save in database
            pool.getConnection(function (err, connection) {
                if (err) {
                    console.log(err);
                    return;
                }
                connection.query("INSERT INTO feedbacks (username, email, feedback, source) VALUES ('"+ name +"','"+ email +"','"+ feedbackmsg +"','Quiz System')", function (err, nextquestion) {
                    if (err) throw err;

                    var app = require("express")();
                    var fs = require('fs');
                    var logger = require("morgan");
                    var nodemailer = require('nodemailer');
                    var mg = require('nodemailer-mailgun-transport');
                    var bodyParser = require('body-parser');
                    var nconf = require('nconf');
                    var auth =  require('../config.json');

                    app.use(bodyParser.urlencoded({ extended: false }));
                    app.use(bodyParser.json());

                    var accessLogStream = fs.createWriteStream(__dirname + '/access.log', { flags: 'a' });
                    app.use(logger('dev'));
                    app.use(logger('combined', { stream: accessLogStream }));

                    // create transporter object capable of sending email using the default SMTP transport
                    var transporter = nodemailer.createTransport(mg(auth));

                    // setup e-mail data with unicode symbols
                    var mailOptions = {
                        from: email, // sender address
                        to: 'vedaquiz@gmail.com', // list of receivers
                        subject: 'Feedback from VedaQuiz Quiz System', // Subject line
                        text: 'Name: ' + name + '\n\n\nEmail: ' + email + '\n\n\nFeedback: ' + feedbackmsg,
                        err: false
                    };
                    // send mail with defined transport object
                    transporter.sendMail(mailOptions, function (error, info) {
                        if (error) {
                            console.log('\nERROR: ' + error+'\n');
                        } else {
                            socket.emit('feedback successful');
                        }
                    });
                });
            });
        });
    });
}