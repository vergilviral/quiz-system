# README #

### Full Project Details ###

Quiz system: 

- Quiz system repo - quiz-system-repo in "Not On Cloud" folder on Mac 
- Hosted on google cloud compute engine (vedaquiz)
- Database on google cloud SQL
- domain name - vedaquiz.com (purchased from 1and1.com at 30th January for 1 year)

Admin Panel:

- Admin panel repo - Admin-panel-with-CI-repo in "MAMP/htdocs" folder on Mac
- Not hosted yet
- Database on google cloud SQL
- domain name - TBA 

Link Shortner System

- Link shortner repo - qveda-link-shortner-repo in "MAMP/htdocs" folder on Mac
- Hosted on ehosts (qveda.co)
- Database on google cloud SQL
- domain name - qveda.co (purchased from 1and1.com at 30th January for 1 year)